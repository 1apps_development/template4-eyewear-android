package com.workdo.glasswear.model

import com.google.gson.annotations.SerializedName

data class CityListModel(

	@field:SerializedName("data")
	val data: ArrayList<CityListData>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class CityListData(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("state_id")
	val stateId: Int? = null,

	@field:SerializedName("country_id")
	val countryId: Int? = null
)
