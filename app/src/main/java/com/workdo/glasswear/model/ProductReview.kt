package com.workdo.glasswear.model

import com.google.gson.annotations.SerializedName

data class ProductReview(

	@field:SerializedName("user_email")
	val userEmail: String? = null,

	@field:SerializedName("sub_title")
	val subTitle: String? = null,

	@field:SerializedName("user_image")
	val userImage: String? = null,

	@field:SerializedName("product_image")
	val productImage: String? = null,

	@field:SerializedName("review")
	val review: String? = null,

	@field:SerializedName("user_name")
	val userName: String? = null,

	@field:SerializedName("rating")
	val rating: Int? = null,

	@field:SerializedName("title")
	val title: String? = null
)
