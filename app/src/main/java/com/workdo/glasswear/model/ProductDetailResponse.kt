package com.workdo.glasswear.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ProductDetailResponse(

    @field:SerializedName("data")
    val data: ProductDetailData? = null,

    @field:SerializedName("count")
    val count: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class ProductDetailData(

    @field:SerializedName("product_image")
    val productImage: ArrayList<ProductImageItem>? = null,

    @field:SerializedName("variant")
    val variant: ArrayList<VariantItem>? = null,

    @field:SerializedName("product_Review")
    val productReview: ArrayList<ProductReview>? = null,

    @field:SerializedName("product_info")
    val productInfo: ProductInfo? = null,

    @field:SerializedName("product_varint")
    val productVarint: ArrayList<Any?>? = null
)

data class ProductImageItem(

    @field:SerializedName("demo_field")
    val demoField: String? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("image_path")
    val imagePath: String? = null,

    @field:SerializedName("product_id")
    val productId: Int? = null,

    @field:SerializedName("theme_id")
    val themeId: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(demoField)
        parcel.writeString(updatedAt)
        parcel.writeString(imagePath)
        parcel.writeValue(productId)
        parcel.writeString(themeId)
        parcel.writeString(createdAt)
        parcel.writeValue(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProductImageItem> {
        override fun createFromParcel(parcel: Parcel): ProductImageItem {
            return ProductImageItem(parcel)
        }

        override fun newArray(size: Int): Array<ProductImageItem?> {
            return arrayOfNulls(size)
        }
    }
}

data class VariantItem(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("value")
    val value: ArrayList<String>? = null,
    var dataItem: ArrayList<VariantDataItem>? = null
)

data class ProductInfo(

    @field:SerializedName("variant_attribute")
    val variantAttribute: String? = null,

    @field:SerializedName("original_price")
    val originalPrice: String? = null,

    @field:SerializedName("default_variant_price")
    val defaultVariantPrice: String? = null,

    @field:SerializedName("tag_api")
    val tagApi: String? = null,

    @field:SerializedName("cover_image_url")
    val coverImageUrl: String? = null,

    @field:SerializedName("discount_amount")
    val discountAmount: Int? = null,

    @field:SerializedName("discount_price")
    val discountPrice: String? = null,

    @field:SerializedName("theme_id")
    val themeId: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("other_description_array")
    val otherDescriptionArray: ArrayList<OtherDescriptionArrayItem>? = null,

    @field:SerializedName("variant_product")
    val variantProduct: Int? = null,

    @field:SerializedName("demo_field")
    val demoField: String? = null,

    @field:SerializedName("subcategory_id")
    val subcategoryId: Int? = null,

    @field:SerializedName("maincategory_id")
    val maincategoryId: Int? = null,

    @field:SerializedName("other_description")
    val otherDescription: String? = null,

    @field:SerializedName("other_description_api")
    val otherDescriptionApi: String? = null,

    @field:SerializedName("variant_id")
    val variantId: String? = null,

    @field:SerializedName("category_id")
    val categoryId: Int? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("in_whishlist")
    val inWhishlist: Boolean? = null,

    @field:SerializedName("final_price")
    val finalPrice: String? = null,
    @field:SerializedName("is_review")
    val is_review: Int? = null,
    @field:SerializedName("retuen_text")
    val retuen_text: String? = null,

    @field:SerializedName("price")
    val price: Int? = null,

    @field:SerializedName("default_variant_id")
    val defaultVariantId: Int? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("tag")
    val tag: String? = null,

    @field:SerializedName("product_stock")
    val productStock: Int? = null,

    @field:SerializedName("slug")
    val slug: String? = null,

    @field:SerializedName("cover_image_path")
    val coverImagePath: String? = null,

    @field:SerializedName("average_rating")
    val averageRating: Int? = null,

    @field:SerializedName("discount_type")
    val discountType: String? = null,

    @field:SerializedName("default_variant_name")
    val defaultVariantName: String? = null,

    @field:SerializedName("in_cart")
    val inCart: Boolean? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class VariantDataItem(
    val name: String? = null,
    var isSelect: Boolean? = null
)

