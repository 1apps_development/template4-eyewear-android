package com.workdo.glasswear.model

import com.google.gson.annotations.SerializedName

data class LandingPageModel(

	@field:SerializedName("data")
	val data: LandingPageData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class HomepageLoyaltyProgram(

	@field:SerializedName("homepage-loyalty-program-sub-text")
	val homepageLoyaltyProgramSubText: String? = null,

	@field:SerializedName("homepage-loyalty-program-button")
	val homepageLoyaltyProgramButton: String? = null,

	@field:SerializedName("homepage-loyalty-program-bg-image")
	val homepageLoyaltyProgramBgImage: String? = null,

	@field:SerializedName("homepage-loyalty-program-title")
	val homepageLoyaltyProgramTitle: String? = null
)

data class HomepagePromotions(

	@field:SerializedName("homepage-promotions-icon-image")
	val homepagePromotionsIconImage: List<String?>? = null
)

data class HomepageBestsellers(

	@field:SerializedName("homepage-bestsellers-heading")
	val homepageBestsellersHeading: String? = null
)

data class HomepageProducts(

	@field:SerializedName("homepage-products-heading")
	val homepageProductsHeading: String? = null
)

data class HomepageNewsletterSection(

	@field:SerializedName("homepage-newsletter-button")
	val homepageNewsletterButton: String? = null,

	@field:SerializedName("homepage-newsletter-sub-text")
	val homepageNewsletterSubText: String? = null,

	@field:SerializedName("homepage-newsletter-title")
	val homepageNewsletterTitle: String? = null
)

data class HomepageHeader(

	@field:SerializedName("homepage-header-title")
	val homepageHeaderTitle: String? = null,

	@field:SerializedName("homepage-header-sub-text")
	val homepageHeaderSubText: String? = null,

	@field:SerializedName("homepage-header-image")
	val homepageHeaderImage: String? = null,

	@field:SerializedName("homepage-header-button")
	val homepageHeaderButton: String? = null
)

data class ThemJson(

	@field:SerializedName("homepage-header")
	val homepageHeader: HomepageHeader? = null,

	@field:SerializedName("homepage-products")
	val homepageProducts: HomepageProducts? = null,

	@field:SerializedName("homepage-categories")
	val homepageCategories: HomepageCategories? = null,

	@field:SerializedName("homepage-loyalty-program")
	val homepageLoyaltyProgram: HomepageLoyaltyProgram? = null,

	@field:SerializedName("homepage-bestsellers")
	val homepageBestsellers: HomepageBestsellers? = null,

	@field:SerializedName("homepage-newsletter-section")
	val homepageNewsletterSection: HomepageNewsletterSection? = null,

	@field:SerializedName("homepage-promotions")
	val homepagePromotions: HomepagePromotions? = null
)

data class HomepageCategories(

	@field:SerializedName("homepage-categories-heading")
	val homepageCategoriesHeading: String? = null
)

data class LandingPageData(

	@field:SerializedName("them_json")
	val themJson: ThemJson? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("loyality_section")
	val loyalitySection: String? = null
)
