package com.workdo.glasswear.model

import com.google.gson.annotations.SerializedName

data class LoyalityModel(

	@field:SerializedName("data")
	val data: LoyalityData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class LoyalityData(

	@field:SerializedName("loyality-program")
	val loyalityProgram: LoyalityProgram? = null
)

data class LoyalityProgram(
	@field:SerializedName("message")
	val message: String? = null,
	@field:SerializedName("loyality-program-description")
	val loyalityProgramDescription: String? = null,

	@field:SerializedName("loyality-program-your-cash")
	val loyalityProgramYourCash: String? = null,

	@field:SerializedName("loyality-program-title")
	val loyalityProgramTitle: String? = null,

	@field:SerializedName("loyality-program-copy-this-link-and-send-to-your-friends")
	val loyalityProgramCopyThisLinkAndSendToYourFriends: String? = null
)
