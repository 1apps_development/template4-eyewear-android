package com.workdo.glasswear.model

import com.google.gson.annotations.SerializedName

data class BestSellersModel(

    @field:SerializedName("data")
    val data: BestSellersData? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("count")
    val count: Int? = null,

    @field:SerializedName("status")
    val status: Int? = null
)


data class BestSellersData(

    @field:SerializedName("per_page")
    val perPage: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("data")
    val data: ArrayList<FeaturedProductsSub>? = null,

    @field:SerializedName("last_page")
    val lastPage: Int? = null,

    @field:SerializedName("next_page_url")
    val nextPageUrl: String? = null,

    @field:SerializedName("prev_page_url")
    val prevPageUrl: Any? = null,

    @field:SerializedName("first_page_url")
    val firstPageUrl: String? = null,

    @field:SerializedName("path")
    val path: String? = null,

    @field:SerializedName("total")
    val total: Int? = null,

    @field:SerializedName("last_page_url")
    val lastPageUrl: String? = null,

    @field:SerializedName("from")
    val from: Int? = null,

    @field:SerializedName("links")
    val links: List<LinksItem?>? = null,

    @field:SerializedName("to")
    val to: Int? = null,

    @field:SerializedName("current_page")
    val currentPage: Int? = null
)

data class DataItem(

    @field:SerializedName("variant_attribute")
    val variantAttribute: Any? = null,

    @field:SerializedName("original_price")
    val originalPrice: Int? = null,

    @field:SerializedName("default_variant_price")
    val defaultVariantPrice: Int? = null,

    @field:SerializedName("tag_api")
    val tagApi: String? = null,

    @field:SerializedName("cover_image_url")
    val coverImageUrl: String? = null,

    @field:SerializedName("discount_amount")
    val discountAmount: Int? = null,

    @field:SerializedName("discount_price")
    val discountPrice: Int? = null,

    @field:SerializedName("theme_id")
    val themeId: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("other_description_array")
    val otherDescriptionArray: ArrayList<OtherDescriptionArrayItem>? = null,

    @field:SerializedName("variant_product")
    val variantProduct: Int? = null,

    @field:SerializedName("demo_field")
    val demoField: String? = null,

    @field:SerializedName("subcategory_id")
    val subcategoryId: Int? = null,

    @field:SerializedName("maincategory_id")
    val maincategoryId: Int? = null,

    @field:SerializedName("other_description")
    val otherDescription: String? = null,

    @field:SerializedName("other_description_api")
    val otherDescriptionApi: String? = null,

    @field:SerializedName("variant_id")
    val variantId: Any? = null,

    @field:SerializedName("category_id")
    val categoryId: Int? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("in_whishlist")
    val inWhishlist: Boolean? = null,

    @field:SerializedName("final_price")
    val finalPrice: Int? = null,

    @field:SerializedName("price")
    val price: Int? = null,

    @field:SerializedName("default_variant_id")
    val defaultVariantId: Int? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("tag")
    val tag: String? = null,

    @field:SerializedName("product_stock")
    val productStock: Int? = null,

    @field:SerializedName("slug")
    val slug: String? = null,

    @field:SerializedName("cover_image_path")
    val coverImagePath: String? = null,

    @field:SerializedName("average_rating")
    val averageRating: Int? = null,

    @field:SerializedName("discount_type")
    val discountType: String? = null,

    @field:SerializedName("in_cart")
    val inCart: Boolean? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

