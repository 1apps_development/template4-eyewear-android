package com.workdo.glasswear.model

import com.google.gson.annotations.SerializedName

data class AddToCartModel(

	@field:SerializedName("data")
	val data: AddtoCartData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class AddtoCartData(

	@field:SerializedName("count")
	val count: Int? = null,

	@field:SerializedName("message")
	val message: String? = null
)
