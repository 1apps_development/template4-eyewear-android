package com.workdo.glasswear.model

import com.google.gson.annotations.SerializedName

data class RegisterModel(

    @field:SerializedName("data")
    val data: Data? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class Data(
    @field:SerializedName("infomation")
    val infomation: String? = null,

    @field:SerializedName("address")
    val address: String? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("mobile")
    val mobile: String? = null,

    @field:SerializedName("postcode")
    val postcode: String? = null,

    @field:SerializedName("last_name")
    val lastName: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("device_type")
    val deviceType: String? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("firebase_token")
    val firebaseToken: String? = null,

    @field:SerializedName("token_type")
    val tokenType: String? = null,

    @field:SerializedName("token")
    val token: String? = null,

    @field:SerializedName("demo_field")
    val demoField: String? = null,

    @field:SerializedName("register_type")
    val registerType: String? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,




    @field:SerializedName("first_name")
    val firstName: String? = null,

    @field:SerializedName("them_json")
    val themJson: ThemJson? = null,

    @field:SerializedName("defaulte_address_id")
    val defaulteAddressId: Int? = null,



    @field:SerializedName("company_name")
    val companyName: String? = null,

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("state_id")
    val stateId: String? = null,

    @field:SerializedName("country_id")
    val countryId: String? = null,

    @field:SerializedName("email")
    val email: String? = null
)
