package com.workdo.glasswear.model

import com.google.gson.annotations.SerializedName

data class LandingPageModel1(

	@field:SerializedName("data")
	val data: LandingPageData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

/*
data class LandingPageData(

	@field:SerializedName("them_json")
	val themJson: ThemJson? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("loyality_section")
	val loyalitySection: String? = null
)

data class ThemJson(

	@field:SerializedName("homepage-header")
	val homepageHeader: HomepageHeader? = null,

	@field:SerializedName("homepage-newsletter")
	val homepageNewsletter: HomepageNewsletter? = null,

	@field:SerializedName("homepage-banner")
	val homepageBanner: HomepageBanner? = null,

	@field:SerializedName("homepage-best-product")
	val homepageBestProduct: HomepageBestProduct? = null
)

data class HomepageBestProduct(

	@field:SerializedName("homepage-best-product-image")
	val homepageBestProductImage: String? = null,

	@field:SerializedName("homepage-best-product-btn-text")
	val homepageBestProductBtnText: String? = null,

	@field:SerializedName("homepage-best-product-sub-text")
	val homepageBestProductSubText: String? = null,

	@field:SerializedName("homepage-best-product-title")
	val homepageBestProductTitle: String? = null
)

data class HomepageHeader(

	@field:SerializedName("homepage-header-bg-image")
	val homepageHeaderBgImage: String? = null,

	@field:SerializedName("homepage-header-title")
	val homepageHeaderTitle: String? = null,

	@field:SerializedName("homepage-header-sub-text")
	val homepageHeaderSubText: String? = null
)

data class HomepageBanner(

	@field:SerializedName("homepage-banner-bg-image")
	val homepageBannerBgImage: String? = null,

	@field:SerializedName("homepage-banner-title")
	val homepageBannerTitle: String? = null,

	@field:SerializedName("homepage-banner-sub-text")
	val homepageBannerSubText: String? = null
)

data class HomepageNewsletter(

	@field:SerializedName("homepage-newsletter-sub-text")
	val homepageNewsletterSubText: String? = null,

	@field:SerializedName("homepage-newsletter-title")
	val homepageNewsletterTitle: String? = null,

	@field:SerializedName("homepage-newsletter-description")
	val homepageNewsletterDescription: String? = null
)
*/
