package com.workdo.glasswear.api

import com.google.gson.annotations.SerializedName

data class PaymentSheetModel(

	@field:SerializedName("clientSecret")
	val clientSecret: String? = null
)
