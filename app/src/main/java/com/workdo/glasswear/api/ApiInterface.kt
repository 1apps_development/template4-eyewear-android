package com.workdo.glasswear.api

import com.workdo.glasswear.adapter.TrendingCategoriModel
import com.workdo.glasswear.model.*
import com.workdo.glasswear.remote.NetworkResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ApiInterface {

    @POST("eyewear")
    suspend fun getApiurl(@Body map: HashMap<String, String>): NetworkResponse<ApiModel, SingleResponse>

    @POST("extra-url")
    suspend fun extraUrl(@Body map: HashMap<String, String>): NetworkResponse<CommonModel, SingleResponse>

    //Todo login api
    @POST("login")
    suspend fun getLogin(@Body map: HashMap<String, String>): NetworkResponse<LoginModel, SingleResponse>

    @POST("register")
    suspend fun setRegistration(@Body map: HashMap<String, String>): NetworkResponse<RegisterModel, SingleResponse>

    @POST("fargot-password-send-otp")
    suspend fun setforgotpasswordsendotp(@Body map: HashMap<String, String>): NetworkResponse<FargotPasswordSendOtpModel, SingleResponse>

    @POST("fargot-password-verify-otp")
    suspend fun setforgotpasswordverifyotp(@Body map: HashMap<String, String>): NetworkResponse<FargotPasswordSendOtpModel, SingleResponse>

    @POST("fargot-password-save")
    suspend fun setforgotpasswordsave(@Body map: HashMap<String, String>): NetworkResponse<FargotPasswordSendOtpModel, SingleResponse>

    @POST("update-user-image")
    @Multipart
    suspend fun setUpdateUserImage(
        @Part("theme_id") themeId:RequestBody,
        @Part("user_id") userId: RequestBody,
        @Part profileimage: MultipartBody.Part?
    ): NetworkResponse<EditProfileUpdateModel, SingleResponse>

    @POST("logout")
    suspend fun setLogout(@Body map: HashMap<String, String>): NetworkResponse<SingleResponse, SingleResponse>


    @POST("currency")
    suspend fun setcurrency(@Body map: HashMap<String, String>): NetworkResponse<CommonModel, SingleResponse>

    @POST("landingpage")
    suspend fun setLandingPage(@Body map: HashMap<String, String>): NetworkResponse<LandingPageModel1, SingleResponse>

    @POST("tranding-category")
    suspend fun setTrandingCategory(@Body map: HashMap<String, String>): NetworkResponse<TrendingCategoriModel, SingleResponse>

    @POST("tranding-category-product")
    suspend fun setTrendingProduct(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<FeaturedProductsSubModel, SingleResponse>

    @POST("bestseller")
    suspend fun setBestSeller(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<BestSellersModel, SingleResponse>

    @POST("bestseller-guest")
    suspend fun setBestSellerGuest(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<BestSellersModel, SingleResponse>



    @POST("tranding-category-product-guest")
    suspend fun setTrendingProductGuest(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<FeaturedProductsSubModel, SingleResponse>

    @POST("categorys-product")
    suspend fun setCategorysProduct(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<FeaturedProductsSubModel, SingleResponse>


    @POST("categorys-product-guest")
    suspend fun setCategorysProductGuest(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<FeaturedProductsSubModel, SingleResponse>

    @POST("wishlist")
    suspend fun setWishlist(@Body map: HashMap<String, String>): NetworkResponse<CommonModel, SingleResponse>

    @POST("addtocart")
    suspend fun addtocart(
        @Body map: HashMap<String, String>
    ): NetworkResponse<AddToCartModel, SingleResponse>

    @POST("cart-qty")
    suspend fun cartQty(@Body map: HashMap<String, String>): NetworkResponse<CommonModel, SingleResponse>

  @POST("home-categoty")
    suspend fun setHomeCategorys(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<HomeCategoriesModel, SingleResponse>

    @POST("product_banner")
    suspend fun productBanner(@Body map: HashMap<String, String>): NetworkResponse<ProductBannerModel, SingleResponse>


    @POST("category-list")
    suspend fun getCategoryList(@Body map: HashMap<String, String>): NetworkResponse<CategoryListModel, SingleResponse>

    @POST("wishlist-list")
    suspend fun getwishliast(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<WishListListModel, SingleResponse>

    @POST("search")
    suspend fun searchList(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<FeaturedProductsSubModel, SingleResponse>

    @POST("search-guest")
    suspend fun searchListGuests(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<FeaturedProductsSubModel, SingleResponse>


    @POST("profile-update")
    suspend fun setProfileUpdate(@Body map: HashMap<String, String>): NetworkResponse<EditProfileModel, SingleResponse>

    @POST("change-password")
    suspend fun setChangePassword(@Body map: HashMap<String, String>): NetworkResponse<CommonModel, SingleResponse>
    //
    @POST("add-address")
    suspend fun addAddress(@Body map: HashMap<String, String>): NetworkResponse<AddAddressModel, SingleResponse>

    @POST("address-list")
    suspend fun addressList(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<AddressListModel, SingleResponse>


    @POST("delete-address")
    suspend fun deleteAddress(@Body map: HashMap<String, String>): NetworkResponse<AddAddressModel, SingleResponse>

    @POST("update-address")
    suspend fun updateAddress(@Body map: HashMap<String, String>): NetworkResponse<AddAddressModel, SingleResponse>

    @POST("country-list")
    suspend fun setCountryList(@Body map: HashMap<String, String>): NetworkResponse<CountryModel, SingleResponse>

    @POST("state-list")
    suspend fun setStateList(@Body map: HashMap<String, String>): NetworkResponse<StateListModel, SingleResponse>

    @POST("city-list")
    suspend fun setCityList(@Body map: HashMap<String, String>): NetworkResponse<CityListModel, SingleResponse>

    @POST("navigation")
    suspend fun navigation(@Body map: HashMap<String, String>): NetworkResponse<MenulistModel, SingleResponse>

    @POST("order-list")
    suspend fun getOrderList(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<OrderHistoryListModel, SingleResponse>

    @POST("order-detail")
    suspend fun getOrderDetail(
        @Body map: HashMap<String, String>
    ): NetworkResponse<OrderDetailsModel, SingleResponse>

    @POST("order-status-change")
    suspend fun orderStatusChanges(@Body map: HashMap<String, String>): NetworkResponse<CommonModel, SingleResponse>

    @POST("product-return")
    suspend fun orderProductReturn(@Body map: HashMap<String, String>): NetworkResponse<CommonModel, SingleResponse>

    @POST("return-order-list")
    suspend fun getReturnOrderList(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<ReturnOrderModel, SingleResponse>

    @POST("loyality-reward")
    suspend fun loyalityReward(@Body map: HashMap<String, String>): NetworkResponse<CommonModel, SingleResponse>

    @POST("loyality-program-json")
    suspend fun loyalityProgramJson(@Body map: HashMap<String, String>): NetworkResponse<LoyalityModel, SingleResponse>

    //
    @POST("product-rating")
    suspend fun productRating(
        @Body map: HashMap<String, String>
    ): NetworkResponse<CommonModel, SingleResponse>

    @POST("notify_user")
    suspend fun notifyUser(@Body map: HashMap<String, String>): NetworkResponse<CommonModel, SingleResponse>

   @POST("check-variant-stock")
    suspend fun variantStock(@Body map: HashMap<String, String>): NetworkResponse<VariantStockResponse, SingleResponse>

    @POST("product-detail-guest")
    suspend fun productDetailGuest(@Body map: HashMap<String, String>): NetworkResponse<ProductDetailResponse, SingleResponse>

    @POST("product-detail")
    suspend fun productDetail(@Body map: HashMap<String, String>): NetworkResponse<ProductDetailResponse, SingleResponse>

    //
    @POST("cart-check")
    suspend fun cartcheck(@Body map: HashMap<String, String>): NetworkResponse<CouponModel, SingleResponse>

    @POST("tax-guest")
    suspend fun taxGuest(@Body map: HashMap<String, String>): NetworkResponse<TaxGuestResponse, SingleResponse>

    @POST("apply-coupon")
    suspend fun applyCoupon(@Body map: HashMap<String, String>): NetworkResponse<CouponModel, SingleResponse>

    @POST("cart-list")
    suspend fun cartList(@Body map: HashMap<String, String>): NetworkResponse<CartListModel, SingleResponse>

    @POST("delivery-list")
    suspend fun deliveryList(@Body map: HashMap<String, String>): NetworkResponse<DeliveryModel, SingleResponse>

    @POST("payment-list")
    suspend fun paymentList(@Body map: HashMap<String, String>): NetworkResponse<PyamentListModel, SingleResponse>

    @POST("confirm-order")
    suspend fun confirmOrder(@Body confirmModel: ConfirmModel): NetworkResponse<ConfirmOrderModel, SingleResponse>

    @POST("place-order")
    suspend fun placeOrder(@Body confirmModel: ConfirmModel): NetworkResponse<OrderModel, SingleResponse>

    @POST("place-order-guest")
    suspend fun guestPlaceOrder(@Body requestData: GuestRequest): NetworkResponse<OrderModel, SingleResponse>

    @POST("payment-sheet")
    suspend fun paymentSheet(@Body map: HashMap<String, String>): NetworkResponse<PaymentSheetModel, SingleResponse>

}

