package com.workdo.glasswear.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.glasswear.R
import com.workdo.glasswear.api.ApiClient
import com.workdo.glasswear.databinding.CellProductsBinding
import com.workdo.glasswear.model.FeaturedProductsSub
import com.workdo.glasswear.utils.Constants
import com.workdo.glasswear.utils.ExtensionFunctions.hide
import com.workdo.glasswear.utils.ExtensionFunctions.show
import com.workdo.glasswear.utils.SharePreference
import com.workdo.glasswear.utils.Utils

class SearchListAdapter(
    private val context: Activity,
    private val providerList: ArrayList<FeaturedProductsSub>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<SearchListAdapter.ViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency_name).toString()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = CellProductsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(providerList[position], context, position, itemClick)

    }

    override fun getItemCount(): Int {
        return providerList.size
    }

    inner class ViewHolder(private val binding: CellProductsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: FeaturedProductsSub,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            Log.e("Cuurent", currency.toString())
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.coverImagePath))
                .into(binding.ivfeaturedProduct)
            binding.tvFeaturedProductName.text = data.name.toString()
            binding.tvFeaturedProductPrice.text =Utils.getPrice(data.finalPrice.toString())
            /*binding.tvProductPrice.text =
                Utils.getPrice(data.originalPrice.toString())*/
            binding.tvcurrenytype.text = currency

            binding.tvTag.text = data.tagApi
            if (data.inWhishlist == true) {
                binding.ivWishlist.background =
                    androidx.core.content.res.ResourcesCompat.getDrawable(
                        context.resources,R.drawable.ic_wishlist,
                        null
                    )
            } else {
                binding.ivWishlist.background =
                    androidx.core.content.res.ResourcesCompat.getDrawable(
                        context.resources,R.drawable.ic_diswishlist,
                        null
                    )
            }
            if (Utils.isLogin(context)) {
                binding.ivWishlist.show()
            } else {
                binding.ivWishlist.hide()

            }
            binding.ivAddtocard.setOnClickListener {
                itemClick(position, Constants.CartClick)
            }
            binding.ivWishlist.setOnClickListener {
                itemClick(position, Constants.FavClick)
            }
            itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }


        }
    }


}