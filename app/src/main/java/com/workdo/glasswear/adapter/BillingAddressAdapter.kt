package com.workdo.glasswear.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.glasswear.R
import com.workdo.glasswear.databinding.CellBillingaddressBinding
import com.workdo.glasswear.model.AddressListData
import com.workdo.glasswear.utils.Constants
import com.workdo.glasswear.utils.SharePreference

class BillingAddressAdapter(
    private val context: Activity,
    private val addressList: ArrayList<AddressListData>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<BillingAddressAdapter.AddressViewHolder>() {
    var firsttime = 0
    inner class AddressViewHolder(private val binding: CellBillingaddressBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: AddressListData,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            if (firsttime == 0) {
                for (i in 0 until addressList.size) {
                    addressList[0].isSelect = true
                    binding.clGetAddress.background =
                        ResourcesCompat.getDrawable(context.resources, R.drawable.border_lightpink_10, null)
                }
            } else {

            }
            if (data.isSelect == true) {
                SharePreference.setStringPref(
                    context,
                    SharePreference.Billing_Company_Name,
                    data.companyName.toString()
                )
                SharePreference.setStringPref(
                    context,
                    SharePreference.Billing_Address,
                    data.address.toString()
                )
                SharePreference.setStringPref(
                    context,
                    SharePreference.Billing_Postecode,
                    data.postcode.toString()
                )
                SharePreference.setStringPref(
                    context,
                    SharePreference.Billing_Country,
                    data.countryId.toString()
                )
                SharePreference.setStringPref(
                    context,
                    SharePreference.Billing_State,
                    data.stateId.toString()
                )
                SharePreference.setStringPref(
                    context,
                    SharePreference.Billing_City,
                    data.city.toString()
                )

                SharePreference.setStringPref(
                    context,
                    SharePreference.Delivery_Address,
                    data.address.toString()
                )
                SharePreference.setStringPref(
                    context,
                    SharePreference.Delivery_Postcode,
                    data.postcode.toString()
                )
                SharePreference.setStringPref(
                    context,
                    SharePreference.Delivery_Country,
                    data.countryId.toString()
                )
                SharePreference.setStringPref(
                    context,
                    SharePreference.Delivery_State,
                    data.stateId.toString()
                )
                SharePreference.setStringPref(
                    context,
                    SharePreference.Delivery_City,
                    data.city.toString()
                )
                binding.clGetAddress.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.border_lightpink_10, null)
            }

            if (data.isSelect == true) {
                binding.ivChecked.setImageDrawable(ResourcesCompat.getDrawable(itemView.context.resources,
                    R.drawable.ic_round_checked,null))
                binding.clGetAddress.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.border_lightpink_10, null)
            } else {
                binding.ivChecked.setImageDrawable(ResourcesCompat.getDrawable(itemView.context.resources,
                    R.drawable.ic_round_unchecked,null))
                binding.clGetAddress.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.bg_gray_10, null)
            }
            binding.tvAddressType.text = addressList[position].title
            binding.tvAddress.text =addressList[position].address.plus(",")
                    .plus(" ").plus(
                    addressList[position].city.plus(",").plus(" ").plus(addressList[position].stateName).plus(",")
                        .plus(" ")
                        .plus(addressList[position].countryName).plus(",").plus(" ").plus(" - ")
                        .plus(addressList[position].postcode).plus(".")
                )

            itemView.setOnClickListener {
                firsttime=1
                addressList[0].isSelect = false
                for (element in addressList) {
                    element.isSelect = false

                }
                data.isSelect = true
                notifyDataSetChanged()
                itemClick(position, Constants.ItemClick)

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressViewHolder {
        val view =
            CellBillingaddressBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AddressViewHolder(view)
    }

    override fun onBindViewHolder(holder: AddressViewHolder, position: Int) {
        holder.bind(addressList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return addressList.size
    }
}