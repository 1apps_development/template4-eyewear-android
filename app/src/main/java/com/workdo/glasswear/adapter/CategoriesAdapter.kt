package com.workdo.glasswear.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.glasswear.R
import com.workdo.glasswear.model.CategoriesModel

class CategoriesAdapter(
    var context: Activity,
    private val mList: List<CategoriesModel>,
    private val onFilterClick: (String, String) -> Unit
) : RecyclerView.Adapter<CategoriesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_categories, parent, false)

        return ViewHolder(view)
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val categoriesModel = mList[position]
        if (categoriesModel.id==0){
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.bg_green_15, null)
            holder.textView.setTextColor(
                AppCompatResources.getColorStateList(
                    context, R.color.white
                )
            )
        }else{
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.bg_gray_15, null)
            holder.textView.setTextColor(
                AppCompatResources.getColorStateList(
                    context, R.color.darkgray
                )
            )

        }
        holder.textView.text = categoriesModel.cateName
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val textView: TextView = itemView.findViewById(R.id.tvCategoriesname)
        val card: ConstraintLayout = itemView.findViewById(R.id.cl)
    }

    private fun onFilterClick(id: String, name: String) {
        onFilterClick.invoke(id, name)
    }
}