package com.workdo.glasswear.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.workdo.glasswear.R
import com.workdo.glasswear.model.CategoriesModel

class LoyalityAdapter(
    var context: Activity,
    private val mList: List<CategoriesModel>,
    private val onFilterClick: (String, String) -> Unit
) : RecyclerView.Adapter<LoyalityAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_order_history, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val categoriesModel = mList[position]
        holder.orderid.text = categoriesModel.cateName
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val orderid: TextView = itemView.findViewById(R.id.tvOrderId)
    }

    private fun onFilterClick(id: String, name: String) {
        onFilterClick.invoke(id, name)
    }
}