package com.workdo.glasswear.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.glasswear.R
import com.workdo.glasswear.api.ApiClient
import com.workdo.glasswear.databinding.CellOrderdetailsBinding
import com.workdo.glasswear.model.ProductItem
import com.workdo.glasswear.utils.Constants
import com.workdo.glasswear.utils.ExtensionFunctions.hide
import com.workdo.glasswear.utils.ExtensionFunctions.show
import com.workdo.glasswear.utils.SharePreference
import com.workdo.glasswear.utils.Utils

class OrderDetailsListAdapter(
    private val context: Activity,
    private val orderlist: ArrayList<ProductItem>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<OrderDetailsListAdapter.WishlistViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency)

    inner class WishlistViewHolder(private val binding: CellOrderdetailsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: ProductItem,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            if (data.returnStatus == "1") {
                binding.clReturnOrder.hide()
            } else {
                binding.clReturnOrder.show()
            }
            binding.tvWishlist.text = data.name
            binding.tvVariantName.text = data.variantName
            binding.tvPrice.text =
                currency.plus(data.finalPrice.toString().let { Utils.getPrice(it) })
            binding.tvQty.text = context.getString(R.string.qty).plus(" ").plus(data.qty)

            Glide.with(context)
                .load(ApiClient.ImageURL.BASE_URL.plus(data.image)).into(binding.ivWishList)
            binding.clReturnOrder.setOnClickListener {
                itemClick(position, Constants.ProductReturn)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WishlistViewHolder {
        val view =
            CellOrderdetailsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return WishlistViewHolder(view)
    }

    override fun onBindViewHolder(holder: WishlistViewHolder, position: Int) {
        holder.bind(orderlist[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return orderlist.size
    }
}