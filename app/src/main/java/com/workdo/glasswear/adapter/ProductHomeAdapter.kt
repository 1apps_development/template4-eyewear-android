package com.workdo.glasswear.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.glasswear.R
import com.workdo.glasswear.model.CategoriesModel
import com.workdo.glasswear.ui.activity.ActProductDetails


class ProductHomeAdapter(
    var context: Activity,
    private val mList: List<CategoriesModel>,
    private val onFilterClick: (String, String) -> Unit
) : RecyclerView.Adapter<ProductHomeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_productshome, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val categoriesModel = mList[position]
        if (categoriesModel.image==0){}else{
        Glide.with(context)
            .load(categoriesModel.image).into(holder.image)}
        holder.itemView.setOnClickListener {
             val intent = Intent(context, ActProductDetails::class.java)
             context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val image: ImageView = itemView.findViewById(R.id.ivProduct)
    }

    private fun onFilterClick(id: String, name: String) {
        onFilterClick.invoke(id, name)
    }
}