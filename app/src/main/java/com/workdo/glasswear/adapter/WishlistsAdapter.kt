package com.workdo.glasswear.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.glasswear.api.ApiClient
import com.workdo.glasswear.databinding.CellWishlistBinding
import com.workdo.glasswear.model.WishListDataItem
import com.workdo.glasswear.utils.Constants
import com.workdo.glasswear.utils.SharePreference
import com.workdo.glasswear.utils.Utils

class WishlistsAdapter(
    private val context: Activity,
    private val wishlist: ArrayList<WishListDataItem>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<WishlistsAdapter.WishlistViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency)

    inner class WishlistViewHolder(private val binding: CellWishlistBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: WishListDataItem, context: Activity, position: Int, itemClick: (Int, String) -> Unit) = with(binding)
        {

            binding.tvWishlist.text = data.productName
            binding.tvVariantName.text = data.variantName
            binding.tvPrice.text = currency.plus(Utils.getPrice(data.finalPrice.toString()))
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.productImage)).into(binding.ivWishList)
            binding.tvDelete.setOnClickListener {
                itemClick(position, Constants.ItemDelete)
            }
            binding.tvAddtocart.setOnClickListener {
                itemClick(position, Constants.CartClick)
            }
            binding.cl1.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WishlistViewHolder {
        val view = CellWishlistBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return WishlistViewHolder(view)
    }

    override fun onBindViewHolder(holder: WishlistViewHolder, position: Int) {
        holder.bind(wishlist[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {
        return wishlist.size
    }
}