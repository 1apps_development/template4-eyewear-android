package com.workdo.glasswear.adapter

import android.app.Activity
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.glasswear.R
import com.workdo.glasswear.api.ApiClient
import com.workdo.glasswear.databinding.CellAllCategoriesBinding
import com.workdo.glasswear.databinding.CellProductlistingBinding
import com.workdo.glasswear.model.CategorylistData
import com.workdo.glasswear.model.HomeCategoriesItem
import com.workdo.glasswear.utils.Constants

class CategoriesListAdapter(
    private val context: Activity,
    private val categoryList: ArrayList<HomeCategoriesItem>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<CategoriesListAdapter.AddressViewHolder>() {
    var firsttime = 0

    inner class AddressViewHolder(private val binding: CellProductlistingBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: HomeCategoriesItem,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            if (firsttime == 0) {
                for (i in 0 until categoryList.size) {
                    categoryList[0].isSelect = true
                }
            }
            if (data.isSelect) {
                binding.card.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.bglightpinkr_10, null)
                binding.tvProductName.setTextColor(ContextCompat.getColor(context, R.color.white))
                binding.ivCart.imageTintList =
                    ColorStateList.valueOf(
                        ResourcesCompat.getColor(
                            context.resources,
                            R.color.white,
                            null
                        )
                    )
            } else {
                binding.card.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.border_lightpink_10, null)
                binding.tvProductName.setTextColor(ContextCompat.getColor(context, R.color.black))
                binding.ivCart.imageTintList =
                    ColorStateList.valueOf(
                        ResourcesCompat.getColor(
                            context.resources,
                            R.color.darkgray,
                            null
                        )
                    )
            }
            binding.tvProductName.text = data.name
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.iconPath))
                .into(binding.ivCart)
            itemView.setOnClickListener {
                firsttime = 1
                categoryList[0].isSelect = false
                for (element in categoryList) {
                    element.isSelect = false
                }
                data.isSelect = true
                notifyDataSetChanged()

                itemClick(position, Constants.ItemClick)
            }


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressViewHolder {
        val view =
            CellProductlistingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AddressViewHolder(view)
    }

    override fun onBindViewHolder(holder: AddressViewHolder, position: Int) {
        holder.bind(categoryList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return categoryList.size
    }
}