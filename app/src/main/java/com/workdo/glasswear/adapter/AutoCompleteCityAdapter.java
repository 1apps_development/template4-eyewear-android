package com.workdo.glasswear.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.workdo.glasswear.R;
import com.workdo.glasswear.model.CityListData;
import com.workdo.glasswear.utils.OnItemClickListenerCity;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteCityAdapter extends ArrayAdapter<CityListData> {
    private List<CityListData> allPlacesList;
    private List<CityListData> filteredPlacesList;
    private final OnItemClickListenerCity listener;

    public AutoCompleteCityAdapter(@NonNull Context context, @NonNull List<CityListData> placesList, OnItemClickListenerCity listener) {
        super(context, 0, placesList);

        allPlacesList = new ArrayList<>(placesList);
        this.listener = listener;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return placeFilter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.esqueleto_personas, parent, false
            );
        }

        TextView placeLabel = convertView.findViewById(R.id.textviewEdad);

        CityListData place = getItem(position);
        if (place != null) {
            placeLabel.setText(place.getName());

            View finalConvertView = convertView;
            placeLabel.setOnClickListener(view ->
            {
                placeLabel.setText(place.getName());
                Log.e("name", place.getName());
                listener.onItemClickCity(place);
            });

        }

        return convertView;
    }

    private Filter placeFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            filteredPlacesList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredPlacesList.addAll(allPlacesList);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (CityListData place : allPlacesList) {
                    if (place.getName().toLowerCase().contains(filterPattern)) {
                        filteredPlacesList.add(place);
                    }
                }
            }

            results.values = filteredPlacesList;
            results.count = filteredPlacesList.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            addAll((List) results.values);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((CityListData) resultValue).getName();
        }
    };
}
