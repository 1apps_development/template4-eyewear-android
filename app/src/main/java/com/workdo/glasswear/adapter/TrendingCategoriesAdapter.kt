package com.workdo.glasswear.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.glasswear.R

class TrendingCategoriesAdapter(
    var context: Activity,
    private val featuredList: List<TrendingCategoriData>,
    private val onClick: (String, String) -> Unit
) : RecyclerView.Adapter<TrendingCategoriesAdapter.ViewHolder>() {
    var firsttime = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_categories, parent, false)

        return ViewHolder(view)
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val categoriesModel = featuredList[position]
        if (firsttime == 0) {
            for (i in featuredList.indices) {
                featuredList[0].isSelect = true
            }
        }
        if (categoriesModel.isSelect) {
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.bglightpinkr_10, null)
            holder.textView.setTextColor(ContextCompat.getColor(context, R.color.white))
        } else {
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.appcolorborder_10, null)
            holder.textView.setTextColor(ContextCompat.getColor(context, R.color.black))
        }
        holder.textView.text = categoriesModel.name
        holder.itemView.setOnClickListener {
            firsttime = 1
            featuredList[0].isSelect = false
            categoriesModel.isFirstPositio = false
            for (element in featuredList) {
                element.isSelect = false
            }
            categoriesModel.isSelect = true
            notifyDataSetChanged()

            onClick(
                categoriesModel.id.toString(),
                categoriesModel.name.toString()
            )
        }
    }

    override fun getItemCount(): Int {
        return featuredList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val textView: TextView = itemView.findViewById(R.id.tvCategoriesname)
        val card: ConstraintLayout = itemView.findViewById(R.id.cl)
    }

    private fun onClick(id: String, name: String) {
        onClick.invoke(id, name)
    }
}