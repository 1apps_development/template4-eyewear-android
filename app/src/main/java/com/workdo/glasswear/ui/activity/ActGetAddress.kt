package com.workdo.glasswear.ui.activity

import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.workdo.glasswear.R
import com.workdo.glasswear.adapter.AddressAdapter
import com.workdo.glasswear.api.ApiClient
import com.workdo.glasswear.base.BaseActivity
import com.workdo.glasswear.databinding.ActGetAddressBinding
import com.workdo.glasswear.model.AddressListData
import com.workdo.glasswear.remote.NetworkResponse
import com.workdo.glasswear.ui.authentication.ActWelCome
import com.workdo.glasswear.utils.Constants
import com.workdo.glasswear.utils.ExtensionFunctions.hide
import com.workdo.glasswear.utils.ExtensionFunctions.show
import com.workdo.glasswear.utils.PaginationScrollListener
import com.workdo.glasswear.utils.SharePreference
import com.workdo.glasswear.utils.Utils
import kotlinx.coroutines.launch

class ActGetAddress : BaseActivity() {
    private lateinit var _binding: ActGetAddressBinding
    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0
    private var addressList = ArrayList<AddressListData>()
    private lateinit var addressAdapter: AddressAdapter
    private var manager: GridLayoutManager? = null


    override fun setLayout(): View = _binding.root
    override fun initView() {
        _binding = ActGetAddressBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.ivBack.setOnClickListener { finish() }
        _binding.tvAddAddress.setOnClickListener { openActivity(ActAddAddress::class.java) }
        manager = GridLayoutManager(this@ActGetAddress, 1, GridLayoutManager.VERTICAL, false)
        pagination()
    }

    private fun pagination() {
        val paginationListener = object : PaginationScrollListener(manager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                callAddressList()
            }
        }
        _binding.rvGetAddress.addOnScrollListener(paginationListener)
    }

    //TODO address list api
    private fun callAddressList() {
        Utils.showLoadingProgress(this@ActGetAddress)
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["user_id"] =
            SharePreference.getStringPref(this@ActGetAddress, SharePreference.userId).toString()
        categoriesProduct["theme_id"]=getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActGetAddress)
                .addressList(currentPage.toString(), categoriesProduct)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addressListResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvGetAddress.show()
                                _binding.tvNoDataFound.hide()
                                this@ActGetAddress.currentPage =
                                    addressListResponse?.currentPage!!.toInt()
                                this@ActGetAddress.total_pages =
                                    addressListResponse.lastPage!!.toInt()
                                addressListResponse.data?.let {
                                    addressList.addAll(it)
                                }
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvGetAddress.hide()
                                _binding.tvNoDataFound.show()
                            }
                            addressAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActGetAddress,
                                addressListResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActGetAddress,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActGetAddress)
                    } else {
                        Utils.errorAlert(
                            this@ActGetAddress,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActGetAddress,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActGetAddress,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO address adapter
    private fun AddressListAdapter(addressList: ArrayList<AddressListData>) {
        _binding.rvGetAddress.layoutManager = manager
        addressAdapter =
            AddressAdapter(this@ActGetAddress, addressList) { i: Int, s: String ->
                if (s == Constants.ItemEdit) {
                    Log.e("EditClick", "Edit")
                    val tyep = 1
                    startActivity(
                        Intent(
                            this@ActGetAddress,
                            ActAddAddress::class.java
                        ).putExtra("addressId", addressList[i].id.toString())
                            .putExtra("company_name", addressList[i].companyName.toString())
                            .putExtra("country_id", addressList[i].countryId.toString())
                            .putExtra("state_id", addressList[i].stateId.toString())
                            .putExtra("city", addressList[i].city.toString())
                            .putExtra("title", addressList[i].title.toString())
                            .putExtra("address", addressList[i].address.toString())
                            .putExtra("postcode", addressList[i].postcode.toString())
                            .putExtra("default_address", addressList[i].defaultAddress.toString())
                            .putExtra("country_name", addressList[i].countryName.toString())
                            .putExtra("state_name", addressList[i].stateName.toString())
                            .putExtra("type", tyep.toString())
                    )
                } else {
                    val deleteAddressMap = HashMap<String, String>()
                    deleteAddressMap["address_id"] = addressList[i].id.toString()
                    deleteAddressMap["theme_id"]=getString(R.string.theme_id)
                    deleteAddressApi(deleteAddressMap, i)
                }
            }
        _binding.rvGetAddress.adapter = addressAdapter
    }

    //TODO address delete api
    private fun deleteAddressApi(deleteAddressMap: HashMap<String, String>, i: Int) {
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActGetAddress)
                .deleteAddress(deleteAddressMap)) {
                is NetworkResponse.Success -> {
                    val deleteaddressResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            addressList.clear()
                            callAddressList()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActGetAddress,
                                deleteaddressResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActGetAddress,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActGetAddress)
                    } else {
                        Utils.errorAlert(
                            this@ActGetAddress,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActGetAddress,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActGetAddress,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        currentPage = 1
        addressList.clear()
        AddressListAdapter(addressList)
        callAddressList()
    }
}