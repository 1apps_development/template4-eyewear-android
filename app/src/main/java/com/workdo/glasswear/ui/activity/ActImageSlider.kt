package com.workdo.glasswear.ui.activity

import android.util.Log
import android.view.View
import com.denzcoskun.imageslider.models.SlideModel
import com.workdo.glasswear.api.ApiClient
import com.workdo.glasswear.base.BaseActivity
import com.workdo.glasswear.databinding.ActImageSliderBinding
import com.workdo.glasswear.model.ProductImageItem

class ActImageSlider : BaseActivity() {
    private lateinit var imageSliderBinding: ActImageSliderBinding
    var imgList: ArrayList<ProductImageItem>? = null

    override fun setLayout(): View = imageSliderBinding.root

    override fun initView() {
        imageSliderBinding = ActImageSliderBinding.inflate(layoutInflater)
        imgList = intent.getParcelableArrayListExtra("imageList")
        Log.e("ImageList",imgList.toString())
        val imageList = ArrayList<SlideModel>()
        for (i in 0 until imgList?.size!!) {
            val slideModel = SlideModel(ApiClient.ImageURL.BASE_URL.plus(imgList!![i].imagePath))
            imageList.add(slideModel)
        }
        imageSliderBinding.imageSlider.setImageList(imageList)
        imageSliderBinding.ivCancel.setOnClickListener {
            finish()
        }
    }
}