package com.workdo.glasswear.ui.authentication

import android.view.View
import com.workdo.glasswear.base.BaseActivity
import com.workdo.glasswear.databinding.ActSuccessfullBinding
import com.workdo.glasswear.ui.activity.MainActivity

class ActSuccessfull : BaseActivity() {
    private lateinit var _binding: ActSuccessfullBinding
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActSuccessfullBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.btnGotoyourstore.setOnClickListener { openActivity(MainActivity::class.java) }
    }
}
