package com.workdo.glasswear.ui.activity

import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.workdo.glasswear.R
import com.workdo.glasswear.adapter.OrderDetailsListAdapter
import com.workdo.glasswear.adapter.TaxListAdapter
import com.workdo.glasswear.api.ApiClient
import com.workdo.glasswear.base.BaseActivity
import com.workdo.glasswear.databinding.ActOrderDetailsBinding
import com.workdo.glasswear.model.*
import com.workdo.glasswear.remote.NetworkResponse
import com.workdo.glasswear.ui.authentication.ActWelCome
import com.workdo.glasswear.utils.Constants
import com.workdo.glasswear.utils.ExtensionFunctions.hide
import com.workdo.glasswear.utils.ExtensionFunctions.show
import com.workdo.glasswear.utils.SharePreference
import com.workdo.glasswear.utils.Utils
import kotlinx.coroutines.launch

class ActOrderDetails : BaseActivity() {
    private lateinit var _binding: ActOrderDetailsBinding
    private var orderDeatils = ArrayList<ProductItem>()
    private lateinit var orderlistAdapter: OrderDetailsListAdapter
    private var taxlist = ArrayList<TaxItem>()
    private lateinit var taxlistAdapter: TaxListAdapter
    private var managerTax: GridLayoutManager? = null
    private var manager: LinearLayoutManager? = null
    var currency = ""
    var currencyName = ""
    var rattingValue = "0"
    var orderStatus = ""
    var trackOrderStatus = ""
    var orderID = ""

    override fun setLayout(): View = _binding.root


    override fun initView() {
        _binding = ActOrderDetailsBinding.inflate(layoutInflater)
        init()
        Log.e("OrdeID", intent.getStringExtra("order_ID").toString())
    }

    private fun init() {
        _binding.ivBack.setOnClickListener { finish() }
        currency =
            SharePreference.getStringPref(this@ActOrderDetails, SharePreference.currency).toString()
        currencyName =
            SharePreference.getStringPref(this@ActOrderDetails, SharePreference.currency_name)
                .toString()
        manager = LinearLayoutManager(this@ActOrderDetails)
        managerTax = GridLayoutManager(this@ActOrderDetails, 1, GridLayoutManager.HORIZONTAL, false)
        _binding.tvTrackOrder.setOnClickListener {
            startActivity(
                Intent(
                    this@ActOrderDetails,
                    ActTrackOrder::class.java
                ).putExtra("trackOrderStatus", trackOrderStatus)
            )
        }
        _binding.btnReturnOrder.setOnClickListener {
            val orderStatusMap = HashMap<String, String>()
            if (orderStatus == "cancel") {
                orderStatusMap["order_id"] = orderID
                orderStatusMap["order_status"] = orderStatus
                orderStatusMap["theme_id"]=getString(R.string.theme_id)
                callOrderStatusChange(orderStatusMap)
            } else if (orderStatus == "return") {
                orderStatusMap["order_id"] = orderID
                orderStatusMap["order_status"] = orderStatus
                orderStatusMap["theme_id"]=getString(R.string.theme_id)
                callOrderStatusChange(orderStatusMap)
            }
        }
    }


    //TODO Product ratting api
    private fun callOrderStatusChange(orderStatusMap: HashMap<String, String>) {
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActOrderDetails)
                .orderStatusChanges(orderStatusMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val rattingResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            callOrderDetailsApi()
                            orderDeatils.clear()
                            orderdetailsAdapter(orderDeatils)
                            taxlist.clear()
                            taxAdapter(taxlist)
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActOrderDetails,
                                rattingResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActOrderDetails,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActOrderDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActOrderDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActOrderDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActOrderDetails,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO order details api
    private fun callOrderDetailsApi() {
        Utils.showLoadingProgress(this@ActOrderDetails)
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["order_id"] =
            intent.getStringExtra("order_ID").toString()
        categoriesProduct["theme_id"]=getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActOrderDetails)
                .getOrderDetail(categoriesProduct)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val orderDetailsResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.product?.size ?: 0) > 0) {
                                _binding.rvOrderDetails.show()
                                _binding.vieww.hide()
                                orderDetailsResponse?.product?.let {
                                    orderDeatils.addAll(it)
                                }
                                orderDetailsResponse?.tax?.let {
                                    taxlist.addAll(it)
                                }
                                _binding.tvOrderID.text = getString(R.string.order).plus("")
                                    .plus(orderDetailsResponse?.orderId.toString())

                            } else {
                                _binding.rvOrderDetails.hide()
                                _binding.vieww.show()
                            }
                            trackOrderStatus = orderDetailsResponse?.orderStatus.toString()
                            orderID = orderDetailsResponse?.id.toString()
                            when (orderDetailsResponse?.orderStatus) {
                                0 -> {
                                    _binding.btnReturnOrder.text = getString(R.string.cancel_order)
                                    _binding.tvRerurnText.hide()
                                    _binding.tvTrackOrder.show()

                                    orderStatus = "cancel"
                                }
                                1 -> {
                                    _binding.btnReturnOrder.text = getString(R.string.return_order)
                                    _binding.tvRerurnText.hide()
                                    _binding.tvTrackOrder.show()

                                    orderStatus = "return"
                                }
                                2 -> {
                                    _binding.btnReturnOrder.hide()
                                    _binding.tvTrackOrder.hide()
                                    _binding.tvRerurnText.show()
                                    _binding.tvRerurnText.text =
                                        orderDetailsResponse.orderStatusMessage
                                }
                                3 -> {
                                    _binding.btnReturnOrder.hide()
                                    _binding.tvTrackOrder.show()
                                    _binding.tvRerurnText.show()
                                    _binding.tvRerurnText.text =
                                        orderDetailsResponse.orderStatusMessage

                                }
                            }


                            orderlistAdapter.notifyDataSetChanged()
                            taxlistAdapter.notifyDataSetChanged()
                            setBillInfo(
                                orderDetailsResponse?.billingInformations,
                                orderDetailsResponse?.deliveryInformations
                            )
                            setpayment(
                                orderDetailsResponse?.paymnet,
                                orderDetailsResponse?.delivery
                            )
                            setcouponinfo(orderDetailsResponse?.couponInfo)
                            setPrice(orderDetailsResponse)
                            _binding.tvTotalPrice.text =
                                currency.plus(Utils.getPrice(orderDetailsResponse?.finalPrice.toString()))
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActOrderDetails,
                                orderDetailsResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActOrderDetails,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActOrderDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActOrderDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActOrderDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActOrderDetails,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //Todo Price
    private fun setPrice(orderDetailsResponse: OrderDetailsData?) {
        _binding.tvSub.text =
            currency.plus(Utils.getPrice(orderDetailsResponse?.subTotal.toString()))


    }

    //TODO coupon info
    private fun setcouponinfo(couponInfo: CouponInfo?) {
        if (couponInfo == null) {
            _binding.tvCouponCode.hide()
            _binding.tvDescountDesc.hide()
            _binding.tvCouponPrice.hide()
            _binding.tvCouponCodeName.hide()
            _binding.view2.hide()
        } else {
            _binding.tvCouponCode.show()
            _binding.view2.show()
            _binding.tvDescountDesc.show()
            _binding.tvCouponPrice.show()
            _binding.tvCouponCodeName.show()
            _binding.tvCouponPrice.text = couponInfo.discountString2
            _binding.tvDescountDesc.text = couponInfo.discountString
            _binding.tvCouponCodeName.text = couponInfo.code
        }
    }

    //TODO payment and delivery image set
    private fun setpayment(paymnet: String?, delivery: String?) {
        Glide.with(this@ActOrderDetails)
            .load(ApiClient.ImageURL.paymentUrl.plus(paymnet)).into(_binding.ivPaymentType)
        Glide.with(this@ActOrderDetails)
            .load(ApiClient.ImageURL.BASE_URL.plus(delivery)).into(_binding.ivDeliveryType)
    }

    //TODO billing info
    private fun setBillInfo(
        billingInformations: BillingInformations?,
        deliveryInformations: DeliveryInformations?
    ) {
        _binding.tvBillUserName.text = billingInformations?.name
        _binding.tvBillUserAddress.text =
            billingInformations?.address.plus(",").plus(" \n").plus(billingInformations?.city)
                .plus(",").plus(" ")
                .plus(billingInformations?.postCode).plus(",").plus(" \n")
                .plus(billingInformations?.state).plus(",")
                .plus("\n ").plus(billingInformations?.country).plus(".")
        _binding.tvBillUserEmail.text = getString(R.string.phone_).plus(billingInformations?.phone)
        _binding.tvBillUserPhone.text = getString(R.string.email_).plus(billingInformations?.email)
        _binding.tvDeliveryUserName.text = deliveryInformations?.name
        _binding.tvDeliveryUserAddress.text =
            deliveryInformations?.address.plus(",").plus(" \n").plus(deliveryInformations?.city)
                .plus(",").plus(" ")
                .plus(deliveryInformations?.postCode).plus(",").plus(" \n")
                .plus(deliveryInformations?.state).plus(",")
                .plus("\n ").plus(deliveryInformations?.country).plus(".")
        _binding.tvDeliveryUserEmail.text =
            getString(R.string.phone_).plus(deliveryInformations?.phone)
        _binding.tvDeliveryUserPhone.text =
            getString(R.string.email_).plus(deliveryInformations?.email)
    }

    override fun onResume() {
        super.onResume()
        orderDeatils.clear()
        callOrderDetailsApi()
        orderdetailsAdapter(orderDeatils)
        taxlist.clear()
        taxAdapter(taxlist)
    }

    //TODO order details adapter
    private fun orderdetailsAdapter(orderDeatils: ArrayList<ProductItem>) {
        _binding.rvOrderDetails.layoutManager = manager
        orderlistAdapter =
            OrderDetailsListAdapter(this@ActOrderDetails, orderDeatils) { i: Int, s: String ->
                if (s == Constants.ProductReturn) {
                    val productReturn = HashMap<String, String>()
                    productReturn["order_id"] = intent.getStringExtra("order_ID").toString()
                    productReturn["product_id"] = orderDeatils[i].productId.toString()
                    productReturn["variant_id"] = orderDeatils[i].variantId.toString()
                    productReturn["theme_id"]=getString(R.string.theme_id)
                    orderProductReturnApi(productReturn)

                }
            }
        _binding.rvOrderDetails.adapter = orderlistAdapter
    }

    private fun orderProductReturnApi(productReturn: HashMap<String, String>) {
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActOrderDetails)
                .orderProductReturn(productReturn)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val rattingResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            callOrderDetailsApi()
                            orderDeatils.clear()
                            orderdetailsAdapter(orderDeatils)
                            taxlist.clear()
                            taxAdapter(taxlist)
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActOrderDetails,
                                rattingResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActOrderDetails,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActOrderDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActOrderDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActOrderDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActOrderDetails,
                        "Something went wrong"
                    )
                }
            }
        }

    }

    //TODO tax details adapter
    private fun taxAdapter(taxList: ArrayList<TaxItem>) {
        _binding.rvTax.layoutManager = managerTax
        taxlistAdapter =
            TaxListAdapter(this@ActOrderDetails, taxList) { i: Int, s: String ->
            }
        _binding.rvTax.adapter = taxlistAdapter
    }
}