package com.workdo.glasswear.ui.activity

import android.os.Build
import android.view.View
import androidx.annotation.RequiresApi
import com.workdo.glasswear.R
import com.workdo.glasswear.base.BaseActivity
import com.workdo.glasswear.databinding.ActTrackOrderBinding

class ActTrackOrder : BaseActivity() {
    private lateinit var _binding: ActTrackOrderBinding
    var trackOrderStatus = ""

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActTrackOrderBinding.inflate(layoutInflater)
        trackOrderStatus = intent.getStringExtra("trackOrderStatus").toString()
        inti()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun inti() {
        _binding.ivBack.setOnClickListener { finish() }
        if (trackOrderStatus=="0"){
            _binding.ivOrderConfirm.setImageResource(R.drawable.ic_option)
            _binding.ivOrderDelivered.setImageResource(R.drawable.ic_unoption)
            _binding.view.setBackgroundColor(getColor(R.color.black))
        }else if (trackOrderStatus=="1"){
            _binding.ivOrderConfirm.setImageResource(R.drawable.ic_option)
            _binding.ivOrderDelivered.setImageResource(R.drawable.ic_option)
            _binding.view.setBackgroundColor(getColor(R.color.black))
        }
    }
}