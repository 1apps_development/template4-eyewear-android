package com.workdo.glasswear.ui.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.workdo.glasswear.R
import com.workdo.glasswear.adapter.*
import com.workdo.glasswear.api.ApiClient
import com.workdo.glasswear.base.BaseFragment
import com.workdo.glasswear.databinding.DlgConfirmBinding
import com.workdo.glasswear.databinding.FragHomeBinding
import com.workdo.glasswear.model.FeaturedProductsSub
import com.workdo.glasswear.model.ProductListItem
import com.workdo.glasswear.remote.NetworkResponse
import com.workdo.glasswear.ui.activity.ActProductDetails
import com.workdo.glasswear.ui.activity.ActShoppingCart
import com.workdo.glasswear.ui.activity.MainActivity
import com.workdo.glasswear.ui.authentication.ActWelCome
import com.workdo.glasswear.ui.option.ActMenu
import com.workdo.glasswear.ui.option.ActSearch
import com.workdo.glasswear.utils.Constants
import com.workdo.glasswear.utils.ExtensionFunctions.hide
import com.workdo.glasswear.utils.ExtensionFunctions.show
import com.workdo.glasswear.utils.PaginationScrollListener
import com.workdo.glasswear.utils.SharePreference
import com.workdo.glasswear.utils.Utils
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.lang.reflect.Type

class FragHome : BaseFragment<FragHomeBinding>() {
    private lateinit var _binding: FragHomeBinding
    private var featuredProductsSubList = ArrayList<FeaturedProductsSub>()
    private lateinit var categoriesProductAdapter: FeaturedAdapter
    private var manager: GridLayoutManager? = null

    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0

    private lateinit var trendingcategoriesAdapter: TrendingCategoriesAdapter
    private var trendingcategoriesList = ArrayList<TrendingCategoriData>()
    private var trendingProductsSubList = ArrayList<FeaturedProductsSub>()
    private lateinit var trendingProductAdapter: TrendingProductAdapter
    private var managerTrending: GridLayoutManager? = null
    internal var isLoadingTrending = false
    internal var isLastPageTrending = false
    private var currentPageTrending = 1
    private var total_pagesTrending: Int = 0

    var trendingcategory_id: String = ""

    private var managerBestsellers: GridLayoutManager? = null
    private var bestsellersList = ArrayList<FeaturedProductsSub>()
    private lateinit var bestSellersAdapter: BestsellerAdapter

    internal var isLoadingBestSeller = false
    internal var isLastPageBestSeller = false
    private var currentPageBestSeller = 1
    private var total_pagesBestSeller: Int = 0

    var count = 1
    override fun initView(view: View) {
        init()
    }

    override fun getBinding(): FragHomeBinding {
        _binding = FragHomeBinding.inflate(layoutInflater)
        return _binding
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun init() {

        _binding.btnBestProductShowNow.setOnClickListener {
            val intent = Intent(requireActivity(), MainActivity::class.java)
            intent.putExtra("pos", "3")
            startActivity(intent)
        }

        _binding.ivCart.setOnClickListener { openActivity(ActShoppingCart::class.java) }
        _binding.ivMenu.setOnClickListener { openActivity(ActMenu::class.java) }

        _binding.edSearch.setOnClickListener {
            openActivity(ActSearch::class.java)
        }
        _binding.clSearch.setOnClickListener {
            openActivity(ActSearch::class.java)
        }

        manager = GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)
        managerTrending =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)

        managerBestsellers =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)

        if (SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)
                .isNullOrEmpty()
        ) {
            SharePreference.setStringPref(requireActivity(), SharePreference.cartCount, "0")
        }
        trendingProductCateAdapter()
        pagination()
        paginationTrending()
        paginationBestSeller()
    }


    //TODO Currency Api calling
    private fun callCurrencyApi() {
        Utils.showLoadingProgress(requireActivity())
        var currencyMap = HashMap<String, String>()
        currencyMap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setcurrency(currencyMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val currencyResponse = response.body.data
                    when (response.body.status) {
                        1 -> {

                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.currency,
                                currencyResponse?.currency.toString()
                            )
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.currency_name,
                                currencyResponse?.currency_name.toString()
                            )
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                currencyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            // openActivity(ActWelCome::class.java)
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    //TODO Header content api calling
    private fun callHeaderContentApi() {
        Utils.showLoadingProgress(requireActivity())
        val theme = HashMap<String, String>()
        theme["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setLandingPage(theme)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val headerContenResponse = response.body.data
                    when (response.body.status) {
                        1 -> {

                            _binding.tvFeaturedCollection.text =
                                headerContenResponse?.themJson?.homepageHeader?.homepageHeaderTitle
                            _binding.tvDesc.text =
                                headerContenResponse?.themJson?.homepageHeader?.homepageHeaderSubText
                            Glide.with(requireActivity())
                                .load(ApiClient.ImageURL.BASE_URL.plus(headerContenResponse?.themJson?.homepageHeader?.homepageHeaderImage))
                                .into(_binding.ivFirstHome)
                            Glide.with(requireActivity())
                                .load(
                                    ApiClient.ImageURL.BASE_URL.plus(
                                        headerContenResponse?.themJson?.homepagePromotions?.homepagePromotionsIconImage?.get(
                                            0
                                        ).toString()
                                    )
                                )
                                .into(_binding.ivBanner)
                            _binding.ivBanner.show()

                            _binding.tvBannerTitle.text =
                                headerContenResponse?.themJson?.homepageProducts?.homepageProductsHeading
                            _binding.tvBannerSubTitle.text =
                                headerContenResponse?.themJson?.homepageHeader?.homepageHeaderSubText

                            Glide.with(requireActivity())
                                .load(
                                    ApiClient.ImageURL.BASE_URL.plus(
                                        headerContenResponse?.themJson?.homepagePromotions?.homepagePromotionsIconImage?.get(
                                            1
                                        ).toString()
                                    )
                                )
                                .into(_binding.ivBestProductImage)

                            _binding.tvBestProductTitle.text =
                                headerContenResponse?.themJson?.homepageCategories?.homepageCategoriesHeading
                            _binding.tvBestProductSubTitle.text =
                                headerContenResponse?.themJson?.homepageHeader?.homepageHeaderSubText
                            _binding.btnBestProductShowNow.text =
                                headerContenResponse?.themJson?.homepageHeader?.homepageHeaderButton

                            if (response.body.data?.loyalitySection == "on") {
                                _binding.tvNewsletterTitle.show()
                                _binding.tvNewsletterSubTitle.show()
                                _binding.clSendMail.show()
                                _binding.chbTermsCondition.show()
                                _binding.tvNewsletterDescription.show()
                                _binding.tvNewsletterTitle.text =
                                    headerContenResponse?.themJson?.homepageNewsletterSection?.homepageNewsletterTitle
                                _binding.tvNewsletterSubTitle.text =
                                    headerContenResponse?.themJson?.homepageNewsletterSection?.homepageNewsletterSubText
                                _binding.tvNewsletterDescription.text =
                                    headerContenResponse?.themJson?.homepageNewsletterSection?.homepageNewsletterButton
                            } else {
                                _binding.tvNewsletterTitle.hide()
                                _binding.tvNewsletterSubTitle.hide()
                                _binding.clSendMail.hide()
                                _binding.chbTermsCondition.hide()
                                _binding.tvNewsletterDescription.hide()
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                headerContenResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                response.body.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    //Categories Product Api calling
    private fun callCategorysProduct() {
        Utils.showLoadingProgress(requireActivity())
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["subcategory_id"] = "0"
        categoriesProduct["maincategory_id"] = "0"
        categoriesProduct["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setCategorysProductGuest(currentPage.toString(), categoriesProduct)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setCategorysProduct(currentPage.toString(), categoriesProduct)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val categoriesProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvFeaturedProduct.show()
                                this@FragHome.currentPage =
                                    categoriesProductResponse?.currentPage!!.toInt()
                                this@FragHome.total_pages =
                                    categoriesProductResponse.lastPage!!.toInt()
                                categoriesProductResponse.data?.let {
                                    featuredProductsSubList.addAll(it)
                                }
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvFeaturedProduct.hide()
                            }
                            categoriesProductAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesProductResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesProductResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    //Categories Product pagination
    private fun pagination() {
        val paginationListener = object : PaginationScrollListener(manager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                callCategorysProduct()
            }
        }
        _binding.rvFeaturedProduct.addOnScrollListener(paginationListener)
    }

    //Adapter set Categories Product
    private fun categoriesProductsAdapter(featuredProductsSubList: ArrayList<FeaturedProductsSub>) {
        _binding.rvFeaturedProduct.layoutManager = manager
        categoriesProductAdapter =
            FeaturedAdapter(requireActivity(), featuredProductsSubList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (featuredProductsSubList[i].inWhishlist == false) {
                            callWishlist(
                                "add",
                                featuredProductsSubList[i].id,
                                "CategoriesProduct",
                                i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                featuredProductsSubList[i].id,
                                "CategoriesProduct", i
                            )
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = featuredProductsSubList[i]
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(data, featuredProductsSubList[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = featuredProductsSubList[i].id.toString()
                        addtocart["variant_id"] =
                            featuredProductsSubList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            featuredProductsSubList[i].id,
                            featuredProductsSubList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, featuredProductsSubList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvFeaturedProduct.adapter = categoriesProductAdapter
    }

    private fun callWishlist(type: String, id: Int?, itemType: String, position: Int) {
        Utils.showLoadingProgress(requireActivity())
        val wishlist = HashMap<String, String>()
        wishlist["user_id"] =
            SharePreference.getStringPref(requireActivity(), SharePreference.userId).toString()
        wishlist["product_id"] = id.toString()
        wishlist["wishlist_type"] = type
        wishlist["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setWishlist(wishlist)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if (type == "add") {
                                when (itemType) {
                                    "CategoriesProduct" -> {
                                        featuredProductsSubList[position].inWhishlist = true
                                        categoriesProductAdapter.notifyItemChanged(position)
                                    }
                                    "BestsellersList" -> {
                                        bestsellersList[position].inWhishlist = true
                                        bestSellersAdapter.notifyItemChanged(position)
                                    }
                                    "TrendingList" -> {
                                        trendingProductsSubList[position].inWhishlist = true
                                        trendingProductAdapter.notifyItemChanged(position)
                                    }
                                }
                            } else {
                                when (itemType) {
                                    "CategoriesProduct" -> {
                                        featuredProductsSubList[position].inWhishlist = false
                                        categoriesProductAdapter.notifyItemChanged(position)
                                    }
                                    "BestsellersList" -> {
                                        bestsellersList[position].inWhishlist = false
                                        bestSellersAdapter.notifyItemChanged(position)
                                    }
                                    "TrendingList" -> {
                                        trendingProductsSubList[position].inWhishlist = false
                                        trendingProductAdapter.notifyItemChanged(position)
                                    }
                                }
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }


    private fun guestUserAddToCart(data: FeaturedProductsSub, id: Int?) {
        Log.e("Position", id.toString())
        val cartData = cartData(data)
        Log.e("cartData2", Gson().toJson(cartData))

        val cartListData = SharePreference.getStringPref(
            requireActivity(),
            SharePreference.GuestCartList
        )
        val type: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type

        var cartList =
            Gson().fromJson<ArrayList<ProductListItem>>(cartListData, type)
        Log.e("cartListBefore2", Gson().toJson(cartList))

        if (cartList == null) {
            //  dlgConfirm(data.name+" "+R.string.addsuccess)
            val cartDataList = ArrayList<ProductListItem>()
            cartDataList.add(cartData)

            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartDataList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartDataList.size.toString()
            )
            _binding.tvCount.text = cartDataList.size.toString()
        } else if (isProductAlreadyAdded(cartList, data.id.toString())) {
            // Utils.errorAlert(requireActivity(), "item already added into cart")
            for (i in 0 until cartList.size) {
                if (cartList[i].productId?.toInt() == id) {
                    val position = cartList[i]
                    Log.e("SelectId", position.toString())
                    dlgAlreadyCart(data.message, data.id, data.defaultVariantId, i)
                }
            }
        } else {
            dlgConfirm(data.name.plus(" ").plus("add successfully."))
            cartList.add(cartData)
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartList.size.toString()
            )
            _binding.tvCount.text = cartList.size.toString()
        }
    }

    private fun isProductAlreadyAdded(cartList: ArrayList<ProductListItem>, id: String): Boolean {
        return cartList.filter { it.productId == id.toString() }.size == 1
    }

    private fun cartData(data: FeaturedProductsSub): ProductListItem {
        return ProductListItem(
            data.coverImagePath.toString(),
            data.defaultVariantId.toString(),
            data.finalPrice.toString(),
            data.discountPrice.toString(),
            data.id.toString(),
            1,
            data.name.toString(),
            "",
            "",
            data.variantName.toString(),
            data.originalPrice.toString()
        )
    }

    private fun addtocartApi(
        addtocart: HashMap<String, String>,
        id: Int?,
        defaultVariantId: Int?,
        i: Int?
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtocart = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.cartCount,
                                addtocart?.count.toString()
                            )
                            setCount(addtocart?.count)
                        }
                        0 -> {
                            dlgAlreadyCart(response.body.data?.message, id, defaultVariantId, i)

                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                addtocart?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    private fun setCount(count: Int?) {
        _binding.tvCount.text = count.toString()
    }


    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(requireActivity())
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setOnClickListener {
            dialog.dismiss()
            openActivity(ActShoppingCart::class.java)
        }
        confirmDialogBinding.tvContinueShopping.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun dlgAlreadyCart(message: String?, id: Int?, defaultVariantId: Int?, i: Int?) {
        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.product_already_again)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val cartqty = java.util.HashMap<String, String>()

            if (Utils.isLogin(requireActivity())) {
                count = 1
                cartqty["product_id"] = id.toString()
                cartqty["user_id"] =
                    SharePreference.getStringPref(
                        requireActivity(),
                        SharePreference.userId
                    ).toString()
                cartqty["variant_id"] = defaultVariantId.toString()
                cartqty["quantity_type"] = "increase"
                cartqty["theme_id"] = getString(R.string.theme_id)
                cartQtyApi(cartqty, "increase")
            } else {
                val cartListData = SharePreference.getStringPref(
                    requireActivity(),
                    SharePreference.GuestCartList
                )
                val type: Type = object : TypeToken<java.util.ArrayList<ProductListItem>>() {}.type

                var cartList =
                    Gson().fromJson<java.util.ArrayList<ProductListItem>>(cartListData, type)
                var count = cartList[i!!].qty?.toInt()!!
                Log.e("Count", count.toString())
                cartList[i!!].qty = count.plus(1)

                manageOfflineData(cartList)
            }
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun manageOfflineData(cartItemList: java.util.ArrayList<ProductListItem>) {

        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.GuestCartList,
            Gson().toJson(cartItemList)
        )
        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.cartCount,
            cartItemList.size.toString()
        )
    }

    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>,
        type: String
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        requireActivity(),
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {
                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(), response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
                else -> {}
            }
        }
    }


    private fun paginationTrending() {
        val paginationListener = object : PaginationScrollListener(managerTrending) {
            override fun isLastPage(): Boolean {
                return isLastPageTrending
            }

            override fun isLoading(): Boolean {
                return isLoadingTrending
            }

            override fun loadMoreItems() {
                isLoadingTrending = true
                currentPageTrending++
                callTrendingCategorysProduct()
            }
        }
        _binding.rvTrandingCategoryProduct.addOnScrollListener(paginationListener)
    }

    //Featured Product Api calling
    private fun callTrendingProduct() {
        Utils.showLoadingProgress(requireActivity())
        val trendingProduct = HashMap<String, String>()
        trendingProduct["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setTrandingCategory(trendingProduct)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val trendingcategorieResponse = response.body.data
                    when (response.body.status) {
                        1 -> {

                            response.body.data?.let { trendingcategoriesList.addAll(it) }

                            if (trendingcategoriesList.size == 0) {
                                _binding.rvTrandingCategory.hide()

                            } else {
                                _binding.rvTrandingCategory.show()

                            }
                            trendingcategoriesAdapter.notifyDataSetChanged()

                            trendingcategory_id = trendingcategoriesList[0].id.toString()
                            Log.e("trendingcategory_id", trendingcategory_id.toString())
                            currentPageTrending = 1
                            isLoadingTrending = false
                            isLastPageTrending = false
                            callTrendingCategorysProduct()

                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingcategorieResponse?.get(0)?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingcategorieResponse?.get(0)?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }


    private fun trendingProductCateAdapter() {
        _binding.rvTrandingCategory.layoutManager =
            GridLayoutManager(requireActivity(), 2, GridLayoutManager.HORIZONTAL, false)
        trendingcategoriesAdapter =
            TrendingCategoriesAdapter(
                requireActivity(),
                trendingcategoriesList
            ) { id: String, name: String ->
                Log.e("CategoriesName", name)
                Log.e("CategoriesName", id.toString())
                trendingProductsSubList.clear()
                trendingcategory_id = id.toString()
                currentPageTrending = 1
                total_pagesTrending = 0
                isLastPageTrending = false
                isLoadingTrending = false
                callTrendingCategorysProduct()
            }
        _binding.rvTrandingCategory.adapter = trendingcategoriesAdapter
    }

    //TODO Categories Product Api calling
    private fun callTrendingCategorysProduct() {
        Utils.showLoadingProgress(requireActivity())
        val trendingProduct = HashMap<String, String>()
        trendingProduct["main_category_id"] = trendingcategory_id
        trendingProduct["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setTrendingProductGuest(currentPageTrending.toString(), trendingProduct)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setTrendingProduct(currentPageTrending.toString(), trendingProduct)
                }


            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val trendingProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvTrandingCategoryProduct.show()

                                currentPageTrending =
                                    trendingProductResponse?.currentPage!!.toInt()
                                total_pagesTrending =
                                    trendingProductResponse.lastPage!!.toInt()

                                trendingProductResponse.data?.let {
                                    trendingProductsSubList.addAll(it)
                                }


//                                 trendingcategoriesList[0].isSelect = true

                                if (currentPageTrending >= total_pagesTrending) {
                                    isLastPageTrending = true
                                }
                                isLoadingTrending = false
                            } else {
                                _binding.rvTrandingCategoryProduct.hide()
                            }
                            trendingProductAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingProductResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingProductResponse?.message.toString()
                            )

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    private fun trendingProductAdapter(trendingProductsSubLists: ArrayList<FeaturedProductsSub>) {
        _binding.rvTrandingCategoryProduct.layoutManager = managerTrending
        trendingProductAdapter =
            TrendingProductAdapter(
                requireActivity(),
                trendingProductsSubLists
            ) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (trendingProductsSubLists[i].inWhishlist == false) {
                            callWishlist(
                                "add", trendingProductsSubLists[i].id, "TrendingList", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                trendingProductsSubLists[i].id,
                                "TrendingList",
                                i
                            )
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = trendingProductsSubList[i]
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(data, trendingProductsSubLists[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = trendingProductsSubLists[i].id.toString()
                        addtocart["variant_id"] =
                            trendingProductsSubLists[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            trendingProductsSubLists[i].id,
                            trendingProductsSubLists[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, trendingProductsSubLists[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvTrandingCategoryProduct.adapter = trendingProductAdapter
    }


    private fun callBestseller() {
        Utils.showLoadingProgress(requireActivity())
        val bestseller = HashMap<String, String>()
        bestseller["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {

            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setBestSellerGuest(currentPageBestSeller.toString(), bestseller)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setBestSeller(currentPageBestSeller.toString(), bestseller)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val bestsellerResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                if (Utils.isLogin(requireActivity())) {
                                    SharePreference.setStringPref(
                                        requireActivity(),
                                        SharePreference.cartCount,
                                        response.body.count.toString()
                                    )
                                    _binding.tvCount.text = response.body.count.toString()
                                }

                                _binding.rvBestsellers.show()
                                _binding.view.hide()
                                /* if (currentPageBestSeller == response.body.data?.lastPage) {
                                     _binding.btnShowMoreBestSellers.hide()
                                 } else {
                                     _binding.btnShowMoreBestSellers.show()
                                 }*/
                                this@FragHome.currentPageBestSeller =
                                    bestsellerResponse?.currentPage!!.toInt()
                                this@FragHome.total_pagesBestSeller =
                                    bestsellerResponse.lastPage!!.toInt()
                                bestsellerResponse.data?.let {
                                    bestsellersList.addAll(it)
                                }


                                if (currentPageBestSeller >= total_pagesBestSeller) {
                                    isLastPageBestSeller = true
                                }
                                isLoadingBestSeller = false
                            } else {
                                _binding.rvBestsellers.hide()
                                _binding.view.show()
                            }
                            bestSellersAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                bestsellerResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                bestsellerResponse?.data?.get(0)?.message.toString()
                            )
                            Utils.setInvalidToekn(requireActivity())
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        getString(R.string.something_went_wrong)
                    )
                }
                else -> {}
            }
        }
    }

    //Adapter set Best seller
    private fun bestsellerAdapter(bestsellersList: ArrayList<FeaturedProductsSub>) {
        _binding.rvBestsellers.layoutManager = managerBestsellers
        bestSellersAdapter =
            BestsellerAdapter(requireActivity(), bestsellersList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (bestsellersList[i].inWhishlist == false) {
                            callWishlist("add", bestsellersList[i].id, "BestsellersList", i)
                        } else {
                            callWishlist("remove", bestsellersList[i].id, "BestsellersList", i)
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = bestsellersList[i]
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(data, bestsellersList[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = bestsellersList[i].id.toString()
                        addtocart["variant_id"] = bestsellersList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            bestsellersList[i].id,
                            bestsellersList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, bestsellersList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvBestsellers.adapter = bestSellersAdapter
    }

    private fun paginationBestSeller() {
        val paginationListener = object : PaginationScrollListener(managerBestsellers) {
            override fun isLastPage(): Boolean {
                return isLastPageBestSeller
            }

            override fun isLoading(): Boolean {
                return isLoadingBestSeller
            }

            override fun loadMoreItems() {
                isLoadingBestSeller = true
                currentPageBestSeller++
                callBestseller()
            }
        }
        _binding.rvBestsellers.addOnScrollListener(paginationListener)
    }


    override fun onResume() {
        super.onResume()

        if (Utils.isCheckNetwork(requireActivity())) {


            lifecycleScope.launch {

                val currency = async {
                    callCurrencyApi()
                }

                currency.await()
                callHeaderContentApi()
                callTrendingProduct()
                trendingcategoriesList.clear()
                currentPage = 1
                featuredProductsSubList.clear()
                categoriesProductsAdapter(featuredProductsSubList)
                callCategorysProduct()

                currentPageTrending = 1
                trendingProductsSubList.clear()
                trendingProductAdapter(trendingProductsSubList)
//        callTrendingCategorysProduct()


                currentPageBestSeller = 1
                bestsellersList.clear()
                bestsellerAdapter(bestsellersList)
                callBestseller()

                Log.e(
                    "Count",
                    SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)
                        .toString()
                )
                _binding.tvCount.text =
                    SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)
            }


        } else {
            Utils.errorAlert(
                requireActivity(),
                resources.getString(R.string.internet_connection_error)
            )
        }
    }

    override fun onStop() {
        super.onStop()
        _binding.ivBanner.hide()
        _binding.view.show()
    }
}