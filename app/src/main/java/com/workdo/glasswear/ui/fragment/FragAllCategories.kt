package com.workdo.glasswear.ui.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.workdo.glasswear.R
import com.workdo.glasswear.adapter.AllCategoryAdapter
import com.workdo.glasswear.api.ApiClient
import com.workdo.glasswear.base.BaseFragment
import com.workdo.glasswear.databinding.FragAllcategoriesBinding
import com.workdo.glasswear.model.HomeCategoriesItem
import com.workdo.glasswear.remote.NetworkResponse
import com.workdo.glasswear.ui.activity.ActCategoryProduct
import com.workdo.glasswear.ui.activity.ActMenuCatProduct
import com.workdo.glasswear.ui.activity.ActShoppingCart
import com.workdo.glasswear.ui.authentication.ActWelCome
import com.workdo.glasswear.utils.Constants
import com.workdo.glasswear.utils.ExtensionFunctions.hide
import com.workdo.glasswear.utils.ExtensionFunctions.show
import com.workdo.glasswear.utils.PaginationScrollListener
import com.workdo.glasswear.utils.SharePreference
import com.workdo.glasswear.utils.Utils
import kotlinx.coroutines.launch

class FragAllCategories : BaseFragment<FragAllcategoriesBinding>() {

    private lateinit var _binding: FragAllcategoriesBinding
    private var managerAllCategories: GridLayoutManager? = null
    private var homeCategoriesList = ArrayList<HomeCategoriesItem>()
    private lateinit var allCategoriesAdapter: AllCategoryAdapter
    internal var isLoadingCategorirs = false
    internal var isLastPageCategorirs = false
    private var currentPageCategorirs = 1
    private var total_pagesCategorirs: Int = 0
    var arraysize = ""

    override fun initView(view: View) {
        managerAllCategories =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.VERTICAL, false)
        init()
        paginationCategories()
    }

    override fun getBinding(): FragAllcategoriesBinding {
        _binding = FragAllcategoriesBinding.inflate(layoutInflater)
        return _binding
    }

    companion object {

    }
    fun showLayout() {
        if (_binding.clRound.visibility==View.GONE)
        {
            _binding.clRound.visibility=View.VISIBLE
        }
    }
    @SuppressLint("NotifyDataSetChanged")
    private fun init() {
        _binding.clRound.show()
        _binding.tvCount.text =
            SharePreference.getStringPref(requireActivity(), SharePreference.cartCount).toString()
        _binding.clcart.setOnClickListener { openActivity(ActShoppingCart::class.java) }
        _binding.scrollView.viewTreeObserver?.addOnScrollChangedListener {
            val view = _binding.scrollView.getChildAt(_binding.scrollView.childCount - 1)
            val diff = view.bottom - (_binding.scrollView.height + _binding.scrollView.scrollY)
            if (diff == 0) {
                _binding.clRound.hide()
            }
        }
    }

    //TODO adapter set Categorirs
    private fun categoriesAdapter(homeCategoriesList: ArrayList<HomeCategoriesItem>) {
        _binding.rvAllCategories.layoutManager = managerAllCategories
        allCategoriesAdapter = AllCategoryAdapter(requireActivity(), homeCategoriesList) { i: Int, s: String ->
                if (s == Constants.ItemClick) {
                    startActivity(Intent(requireActivity(), ActCategoryProduct::class.java).putExtra("maincategory_id", homeCategoriesList[i].categoryId.toString()))
                }
            }
        _binding.rvAllCategories.adapter = allCategoriesAdapter
    }

    //TODO All Categories pagination
    private fun paginationCategories() {

        val paginationListener = object : PaginationScrollListener(managerAllCategories) {
            override fun isLastPage(): Boolean {
                return isLastPageCategorirs
            }

            override fun isLoading(): Boolean {
                return isLoadingCategorirs
            }

            override fun loadMoreItems() {
                isLoadingCategorirs = true
                currentPageCategorirs++
                callCategories()
            }
        }
        _binding.rvAllCategories.addOnScrollListener(paginationListener)
    }


    //TODO  Categories api callig
    private fun callCategories() {
        Utils.showLoadingProgress(requireActivity())
        val categorieshashmap = HashMap<String, String>()
        categorieshashmap["theme_id"] =getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setHomeCategorys(currentPageCategorirs.toString(),categorieshashmap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val categoriesResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                arraysize = categoriesResponse?.data?.size.toString()
                                setRoundCat(categoriesResponse?.data, arraysize)
                                Log.e("LISTSZIE", arraysize)
                                _binding.rvAllCategories.show()
                                _binding.clRound.show()
                                this@FragAllCategories.currentPageCategorirs =
                                    categoriesResponse?.currentPage!!.toInt()
                                this@FragAllCategories.total_pagesCategorirs =
                                    categoriesResponse.lastPage!!.toInt()
                                categoriesResponse.data?.let {
                                    homeCategoriesList.addAll(it)
                                }
                                if (currentPageCategorirs >= total_pagesCategorirs) {
                                    isLastPageCategorirs = true
                                }
                                isLoadingCategorirs = false
                            } else {
                                _binding.rvAllCategories.hide()
                                _binding.clRound.hide()
                            }
                            allCategoriesAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesResponse?.message.toString()
                            )
                            Utils.openWelcomeScreen(requireActivity())
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO set round menu
    private fun setRoundCat(data: ArrayList<HomeCategoriesItem>?, arraysize: String) {
        when (arraysize) {
            "1" -> {
                _binding.clCat1.hide()
                _binding.clCat2.hide()
                _binding.clCat3.hide()
                _binding.clCat4.hide()
                _binding.clCat5.show()
                Glide.with(requireActivity())
                    .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(0)?.iconPath.toString()))
                    .into(_binding.ivCat5)
                _binding.tvCat5.text = data?.get(0)?.name

                _binding.clCat5.setOnClickListener {
                    menuProductNavigation(data,0)

                }
            }
            "2" -> {
                _binding.clCat1.show()
                _binding.clCat2.hide()
                _binding.clCat3.hide()
                _binding.clCat4.show()
                _binding.clCat5.hide()
                Glide.with(requireActivity())
                    .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(0)?.iconPath.toString()))
                    .into(_binding.ivCat1)
                _binding.tvCat1.text = data?.get(0)?.name

                Glide.with(requireActivity())
                    .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(1)?.iconPath.toString()))
                    .into(_binding.ivCat4)
                _binding.tvCat4.text = data?.get(1)?.name

                _binding.clCat1.setOnClickListener {
                    menuProductNavigation(data,0)

                }
                _binding.clCat4.setOnClickListener {
                    menuProductNavigation(data,1)

                }
            }
            "3" -> {
                _binding.clCat1.show()
                _binding.clCat2.hide()
                _binding.clCat3.hide()
                _binding.clCat4.show()
                _binding.clCat5.show()
                Glide.with(requireActivity())
                    .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(0)?.iconPath.toString()))
                    .into(_binding.ivCat1)
                _binding.tvCat1.text = data?.get(0)?.name

                Glide.with(requireActivity())
                    .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(1)?.iconPath.toString()))
                    .into(_binding.ivCat5)
                _binding.tvCat5.text = data?.get(1)?.name

                Glide.with(requireActivity())
                    .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(2)?.iconPath.toString()))
                    .into(_binding.ivCat4)
                _binding.tvCat4.text = data?.get(2)?.name

                _binding.clCat1.setOnClickListener {
                    menuProductNavigation(data,0)

                }
                _binding.clCat5.setOnClickListener {
                    menuProductNavigation(data,1)

                }
                _binding.clCat4.setOnClickListener {

                    menuProductNavigation(data,2)


                }

            }
            "4" -> {
                _binding.clCat1.show()
                _binding.clCat2.show()
                _binding.clCat3.show()
                _binding.clCat4.show()
                _binding.clCat5.hide()
                Glide.with(requireActivity())
                    .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(0)?.iconPath.toString()))
                    .into(_binding.ivCat1)
                _binding.tvCat1.text = data?.get(0)?.name

                Glide.with(requireActivity())
                    .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(1)?.iconPath.toString()))
                    .into(_binding.ivCat2)
                _binding.tvCat2.text = data?.get(1)?.name

                Glide.with(requireActivity())
                    .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(2)?.iconPath.toString()))
                    .into(_binding.ivCat3)
                _binding.tvCat3.text = data?.get(2)?.name

                Glide.with(requireActivity())
                    .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(3)?.iconPath.toString()))
                    .into(_binding.ivCat4)
                _binding.tvCat4.text = data?.get(3)?.name

                _binding.clCat1.setOnClickListener {
                    menuProductNavigation(data,0)

                }
                _binding.clCat2.setOnClickListener {
                    menuProductNavigation(data,1)

                }
                _binding.clCat3.setOnClickListener {
                    menuProductNavigation(data,2)
                }
                _binding.clCat4.setOnClickListener {
                    menuProductNavigation(data,3)



                }
            }
            else -> {
                _binding.clCat1.show()
                _binding.clCat2.show()
                _binding.clCat3.show()
                _binding.clCat4.show()
                _binding.clCat5.hide()
                Glide.with(requireActivity())
                    .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(0)?.iconPath.toString()))
                    .into(_binding.ivCat1)
                _binding.tvCat1.text = data?.get(0)?.name

                Glide.with(requireActivity())
                    .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(1)?.iconPath.toString()))
                    .into(_binding.ivCat2)
                _binding.tvCat2.text = data?.get(1)?.name

                Glide.with(requireActivity())
                    .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(2)?.iconPath.toString()))
                    .into(_binding.ivCat3)
                _binding.tvCat3.text = data?.get(2)?.name

                Glide.with(requireActivity())
                    .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(3)?.iconPath.toString()))
                    .into(_binding.ivCat4)
                _binding.tvCat4.text = data?.get(3)?.name
                _binding.clCat1.setOnClickListener {
                    menuProductNavigation(data,0)

                }
                _binding.clCat2.setOnClickListener {
                    menuProductNavigation(data,1)

                }
                _binding.clCat3.setOnClickListener {
                    menuProductNavigation(data,2)

                }
                _binding.clCat4.setOnClickListener {


                    menuProductNavigation(data,3)


                }
            }
        }
    }

    private fun menuProductNavigation(data: ArrayList<HomeCategoriesItem>?,position:Int)
    {
        val intent = Intent(requireActivity(), ActMenuCatProduct::class.java)
        intent.putExtra("main_id",data?.get(position)?.categoryId.toString())
        intent.putExtra(
            "sub_id","0"
        )
        intent.putExtra("name",data?.get(position)?.name.toString())
        intent.putExtra(
            "menu",
            "menu"
        )
        startActivity(intent)

    }


    override fun onResume() {
        super.onResume()
        currentPageCategorirs = 1
        homeCategoriesList.clear()
        categoriesAdapter(homeCategoriesList)
        callCategories()
        _binding.tvCount.text =
            SharePreference.getStringPref(requireActivity(), SharePreference.cartCount).toString()
    }

}