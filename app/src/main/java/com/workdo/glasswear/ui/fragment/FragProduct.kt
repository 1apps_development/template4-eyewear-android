package com.workdo.glasswear.ui.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.workdo.glasswear.R
import com.workdo.glasswear.adapter.CategoriesListAdapter
import com.workdo.glasswear.adapter.ProductCategoryAdapter
import com.workdo.glasswear.api.ApiClient
import com.workdo.glasswear.databinding.FragProductBinding
import com.workdo.glasswear.base.BaseFragment
import com.workdo.glasswear.databinding.DlgConfirmBinding
import com.workdo.glasswear.model.FeaturedProductsSub
import com.workdo.glasswear.model.HomeCategoriesItem
import com.workdo.glasswear.model.ProductListItem
import com.workdo.glasswear.remote.NetworkResponse
import com.workdo.glasswear.ui.activity.ActProductDetails
import com.workdo.glasswear.ui.activity.ActShoppingCart
import com.workdo.glasswear.ui.authentication.ActWelCome
import com.workdo.glasswear.utils.Constants
import com.workdo.glasswear.utils.ExtensionFunctions.hide
import com.workdo.glasswear.utils.ExtensionFunctions.show
import com.workdo.glasswear.utils.PaginationScrollListener
import com.workdo.glasswear.utils.SharePreference
import com.workdo.glasswear.utils.Utils
import kotlinx.coroutines.launch
import java.lang.reflect.Type

class FragProduct : BaseFragment<FragProductBinding>() {
    private lateinit var _binding: FragProductBinding
    private var managerAllCategories: GridLayoutManager? = null
    private var homeCategoriesList = ArrayList<HomeCategoriesItem>()
    private lateinit var allCategoriesAdapter: CategoriesListAdapter
    internal var isLoadingCategorirs = false
    internal var isLastPageCategorirs = false
    private var currentPageCategorirs = 1
    private var total_pagesCategorirs: Int = 0

    private var managerCatProduct: GridLayoutManager? = null
    private var featuredProductsSubList = ArrayList<FeaturedProductsSub>()
    private lateinit var ProductCategoryAdapter: ProductCategoryAdapter
    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0
    var name = ""
    var subID = ""
    var maincategory_id = ""
    var count = 1
    var countItem = ""

    override fun initView(view: View) {
        _binding = FragProductBinding.bind(_binding.root)
        managerAllCategories =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)
        init()
        paginationCategories()
        managerCatProduct =
            GridLayoutManager(requireActivity(), 2, GridLayoutManager.VERTICAL, false)
        nestedScrollViewPagination()
        if (SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)
                .isNullOrEmpty()
        ) {
            SharePreference.setStringPref(requireActivity(), SharePreference.cartCount, "0")
        }
    }

    override fun getBinding(): FragProductBinding {
        _binding = FragProductBinding.inflate(layoutInflater)
        return _binding
    }


    @SuppressLint("NotifyDataSetChanged")
    private fun init() {
        _binding.tvCount.text =
            SharePreference.getStringPref(requireActivity(), SharePreference.cartCount).toString()
        _binding.clcart.setOnClickListener { openActivity(ActShoppingCart::class.java) }
        productBannerApi()
    }


    //TODO product banner api
    private fun productBannerApi() {
        Utils.showLoadingProgress(requireActivity())
        val productBanner = HashMap<String, String>()
        productBanner["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .productBanner(productBanner)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val bannersResponse = response.body.data?.themJson?.productsHeaderBanner
                    when (response.body.status) {
                        1 -> {
                            Glide.with(requireActivity())
                                .load(ApiClient.ImageURL.BASE_URL.plus(bannersResponse?.productsHeaderBanner))
                                .into(_binding.ivProductBanner)
                            _binding.tvmess.text =
                                bannersResponse?.productsHeaderBannerTitleText.toString()
                        }
                        0 -> {

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO adapter set Categorirs
    private fun categoriesAdapter(homeCategoriesList: ArrayList<HomeCategoriesItem>) {
        _binding.rvAllCategories.layoutManager = managerAllCategories
        allCategoriesAdapter =
            CategoriesListAdapter(requireActivity(), homeCategoriesList) { i: Int, s: String ->
                if (s == Constants.ItemClick) {
                    subID = homeCategoriesList[i].id.toString()
                    maincategory_id = homeCategoriesList[i].maincategoryId.toString()
                    currentPage = 1
                    featuredProductsSubList.clear()
                    callCategorysProduct()
                }
            }
        _binding.rvAllCategories.adapter = allCategoriesAdapter
    }

    //TODO All Categories pagination
    private fun paginationCategories() {

        val paginationListener = object : PaginationScrollListener(managerAllCategories) {
            override fun isLastPage(): Boolean {
                return isLastPageCategorirs
            }

            override fun isLoading(): Boolean {
                return isLoadingCategorirs
            }

            override fun loadMoreItems() {
                isLoadingCategorirs = true
                currentPageCategorirs++
                callCategories()
            }
        }
        _binding.rvAllCategories.addOnScrollListener(paginationListener)
    }

    //TODO  Categories api callig
    private fun callCategories() {
        Utils.showLoadingProgress(requireActivity())
        val categorieshashmap = HashMap<String, String>()
        categorieshashmap["theme_id"] = getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setHomeCategorys(currentPageCategorirs.toString(), categorieshashmap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val categoriesResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvAllCategories.show()
                                homeCategoriesList.add(
                                    HomeCategoriesItem(
                                        name = "All Product",
                                        id = 0
                                    )
                                )

                                this@FragProduct.currentPageCategorirs =
                                    categoriesResponse?.currentPage!!.toInt()
                                this@FragProduct.total_pagesCategorirs =
                                    categoriesResponse.lastPage!!.toInt()
                                categoriesResponse.data?.let {
                                    homeCategoriesList.addAll(it)
                                }
                                subID = homeCategoriesList[0].id.toString()
                                currentPage = 1
                                isLoading = false
                                isLastPage = false
                                featuredProductsSubList.clear()
                                callCategorysProduct()

                                if (currentPageCategorirs >= total_pagesCategorirs) {
                                    isLastPageCategorirs = true
                                }
                                isLoadingCategorirs = false

                            } else {
                                _binding.rvAllCategories.hide()
                            }
                            allCategoriesAdapter.notifyDataSetChanged()

                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun nestedScrollViewPagination() {
        _binding.rvFeaturedProduct.isNestedScrollingEnabled = false
        _binding.scrollView.viewTreeObserver?.addOnScrollChangedListener {
            val view = _binding.scrollView.getChildAt(_binding.scrollView.childCount - 1)
            val diff = view.bottom - (_binding.scrollView.height + _binding.scrollView.scrollY)
            if (diff == 0 && !isLoading && !isLastPage) {
                isLoading = true
                currentPage++
                callCategorysProduct()
            }
        }


    }

    //TODO category product api calling
    private fun callCategorysProduct() {
        Utils.showLoadingProgress(requireActivity())
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["maincategory_id"] = subID
        categoriesProduct["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setCategorysProductGuest(currentPage.toString(), categoriesProduct)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setCategorysProduct(currentPage.toString(), categoriesProduct)
                }
            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val categoriesProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            countItem = categoriesProductResponse?.data?.size.toString()
                            currentPage = categoriesProductResponse?.currentPage!!.toInt()
                            total_pages = categoriesProductResponse.lastPage!!.toInt()
                            categoriesProductResponse.data?.let {
                                featuredProductsSubList.addAll(it)
                            }
                            if (currentPage >= total_pages) {
                                isLastPage = true
                            }
                            isLoading = false
                            if (featuredProductsSubList.size > 0) {
                                _binding.rvFeaturedProduct.show()
                                _binding.view.hide()
                            } else {
                                _binding.rvFeaturedProduct.hide()
                                _binding.view.show()
                            }
                            ProductCategoryAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesProductResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesProductResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                    if (categoriesProductResponse?.to == null) {

                        _binding.tvFoundproduct.text =
                            getString(R.string.found).plus(" ").plus("0").plus(" ")
                                .plus(getString(R.string.product))
                    } else {
                        _binding.tvFoundproduct.text = getString(R.string.found).plus(" ").plus(
                            categoriesProductResponse.total
                        ).plus(" ").plus(getString(R.string.product))
                    }

                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO Adapter set Categories Product
    private fun categoriesProductsAdapter(featuredProductsSubList: ArrayList<FeaturedProductsSub>) {

        _binding.rvFeaturedProduct.layoutManager = managerCatProduct
        countItem = featuredProductsSubList.size.toString()
        ProductCategoryAdapter =
            ProductCategoryAdapter(
                requireActivity(),
                featuredProductsSubList
            ) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (featuredProductsSubList[i].inWhishlist == false) {
                            callWishlist(
                                "add", featuredProductsSubList[i].id, "CategoriesProduct", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                featuredProductsSubList[i].id,
                                "CategoriesProduct",
                                i
                            )
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(
                            featuredProductsSubList[i],
                            featuredProductsSubList[i].id
                        )
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(
                                requireActivity(),
                                SharePreference.userId
                            )
                                .toString()
                        addtocart["product_id"] = featuredProductsSubList[i].id.toString()
                        addtocart["variant_id"] =
                            featuredProductsSubList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            featuredProductsSubList[i].id,
                            featuredProductsSubList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, featuredProductsSubList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvFeaturedProduct.adapter = ProductCategoryAdapter

    }

    private fun guestUserAddToCart(data: FeaturedProductsSub, id: Int?) {

        val cartData = cartData(data)
        Log.e("cartData2", Gson().toJson(cartData))
        val cartListData = SharePreference.getStringPref(
            requireActivity(),
            SharePreference.GuestCartList
        )

        val type: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type
        var cartList = Gson().fromJson<ArrayList<ProductListItem>>(cartListData, type)
        Log.e("cartListBefore2", Gson().toJson(cartList))
        if (cartList == null) {
            val cartDataList = ArrayList<ProductListItem>()
            cartDataList.add(cartData)
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartDataList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartDataList.size.toString()
            )
            setCount(cartDataList.size)
        } else if (isProductAlreadyAdded(cartList, data.id.toString())) {
            //Utils.errorAlert(requireActivity(), "item already added into cart")
            for (i in 0 until cartList.size) {
                if (cartList[i].productId?.toInt() == id) {
                    val position = cartList[i]
                    Log.e("SelectId", position.toString())
                    dlgAlreadyCart(data.message, data.id, data.defaultVariantId, i)
                }
            }

        } else {
            dlgConfirm(data.name.plus(" ").plus("add successfully."))
            cartList.add(cartData)
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartList.size.toString()
            )

            setCount(cartList.size)
        }
    }

    private fun cartData(data: FeaturedProductsSub): ProductListItem {
        return ProductListItem(
            data.coverImagePath.toString(),
            data.defaultVariantId.toString(),
            data.finalPrice.toString(),
            data.discountPrice.toString(),
            data.id.toString(),
            1,
            data.name.toString(),
            "",
            "",
            data.variantName.toString(),
            data.originalPrice.toString()
        )
    }

    private fun isProductAlreadyAdded(cartList: ArrayList<ProductListItem>, id: String): Boolean {
        return cartList.filter { it.productId == id.toString() }.size == 1
    }

    //TODO add to cart api
    private fun addtocartApi(
        addtocart: HashMap<String, String>,
        id: Int?,
        defaultVariantId: Int?,
        i: Int?
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartload = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            setCount(cartload?.count)
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.cartCount,
                                cartload?.count.toString()
                            )
                        }
                        0 -> {
                            dlgAlreadyCart(response.body.data?.message, id, defaultVariantId, i)
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartload?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            activity?.finish()
                            activity?.finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }


    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(requireActivity())
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setOnClickListener {
            dialog.dismiss()
            openActivity(ActShoppingCart::class.java)
        }
        confirmDialogBinding.tvContinueShopping.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun dlgAlreadyCart(message: String?, id: Int?, defaultVariantId: Int?, i: Int?) {
        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.product_already_again)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            dialogInterface.dismiss()
            val cartqty = java.util.HashMap<String, String>()

            if (Utils.isLogin(requireActivity())) {
                count = 1
                cartqty["product_id"] = id.toString()
                cartqty["user_id"] =
                    SharePreference.getStringPref(
                        requireActivity(),
                        SharePreference.userId
                    ).toString()
                cartqty["variant_id"] = defaultVariantId.toString()
                cartqty["quantity_type"] = "increase"
                cartqty["theme_id"] = getString(R.string.theme_id)
                cartQtyApi(cartqty)
            } else {
                val cartListData = SharePreference.getStringPref(
                    requireActivity(),
                    SharePreference.GuestCartList
                )
                val type: Type = object : TypeToken<java.util.ArrayList<ProductListItem>>() {}.type

                val cartList =
                    Gson().fromJson<java.util.ArrayList<ProductListItem>>(cartListData, type)
                val count = cartList[i!!].qty?.toInt()!!
                Log.e("Count", count.toString())
                cartList[i].qty = count.plus(1)

                manageOfflineData(cartList)
            }
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            builder.setCancelable(true)
            dialogInterface.dismiss()
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        requireActivity(),
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {
                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(), response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }

    private fun manageOfflineData(cartItemList: java.util.ArrayList<ProductListItem>) {
        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.GuestCartList,
            Gson().toJson(cartItemList)
        )

        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.cartCount,
            cartItemList.size.toString()
        )

    }

    private fun setCount(count: Int?) {
        _binding.tvCount.text = count.toString()
    }

    //TODO wishlist api
    private fun callWishlist(type: String, id: Int?, itemType: String, position: Int) {
        Utils.showLoadingProgress(requireActivity())
        val wishlist = HashMap<String, String>()
        wishlist["user_id"] =
            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                .toString()
        wishlist["product_id"] = id.toString()
        wishlist["wishlist_type"] = type
        wishlist["theme_id"] = getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setWishlist(wishlist)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if (type == "add") {
                                if (itemType == "CategoriesProduct") {
                                    featuredProductsSubList[position].inWhishlist = true
                                    ProductCategoryAdapter.notifyItemChanged(position)
                                }
                            } else {
                                if (itemType == "CategoriesProduct") {
                                    featuredProductsSubList[position].inWhishlist = false
                                    ProductCategoryAdapter.notifyItemChanged(position)
                                }
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }


    override fun onResume() {
        super.onResume()

        if (Utils.isCheckNetwork(requireActivity())) {
            currentPageCategorirs = 1
            isLastPage = false
            homeCategoriesList.clear()
            categoriesAdapter(homeCategoriesList)
            featuredProductsSubList.clear()
            categoriesProductsAdapter(featuredProductsSubList)
            callCategories()
            _binding.tvCount.text =
                SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)
                    .toString()
        } else {
            Utils.errorAlert(
                requireActivity(),
                resources.getString(R.string.internet_connection_error)
            )
        }


    }

}