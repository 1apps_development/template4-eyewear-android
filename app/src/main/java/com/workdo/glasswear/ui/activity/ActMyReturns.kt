package com.workdo.glasswear.ui.activity

import android.content.Intent
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.workdo.glasswear.R
import com.workdo.glasswear.adapter.ReturnOrderlistAdapter
import com.workdo.glasswear.api.ApiClient
import com.workdo.glasswear.base.BaseActivity
import com.workdo.glasswear.databinding.ActMyReturnsBinding
import com.workdo.glasswear.model.ReturnOrderItem
import com.workdo.glasswear.remote.NetworkResponse
import com.workdo.glasswear.ui.authentication.ActWelCome
import com.workdo.glasswear.utils.Constants
import com.workdo.glasswear.utils.ExtensionFunctions.hide
import com.workdo.glasswear.utils.ExtensionFunctions.show
import com.workdo.glasswear.utils.PaginationScrollListener
import com.workdo.glasswear.utils.SharePreference
import com.workdo.glasswear.utils.Utils
import kotlinx.coroutines.launch

class ActMyReturns : BaseActivity() {
    private lateinit var _binding: ActMyReturnsBinding
    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0
    private var orderList = ArrayList<ReturnOrderItem>()
    private lateinit var returnOrderlistAdapter: ReturnOrderlistAdapter
    private var manager: LinearLayoutManager? = null


    override fun setLayout(): View = _binding.root
    override fun initView() {
        _binding = ActMyReturnsBinding.inflate(layoutInflater)
        init()
    }

    private fun init(){
        manager = LinearLayoutManager(this@ActMyReturns)
        returnOrderListAdapter(orderList)
        nestedScrollViewPagination()
        _binding.ivBack.setOnClickListener { finish() }
    }
    private fun pagination() {
        val paginationListener = object : PaginationScrollListener(manager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }
            override fun isLoading(): Boolean {
                return isLoading
            }
            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                callRetuenOrderList()
            }
        }
        _binding.rvMyReturn.addOnScrollListener(paginationListener)
    }


    private fun nestedScrollViewPagination() {
        _binding.rvMyReturn.isNestedScrollingEnabled = false
        _binding.scrollView.viewTreeObserver?.addOnScrollChangedListener {
            val view = _binding.scrollView.getChildAt(_binding.scrollView.childCount - 1)

            val diff = view.bottom - (_binding.scrollView.height + _binding.scrollView.scrollY)

            if (diff == 0 && !isLoading && !isLastPage) {
                currentPage++
                callRetuenOrderList()
            }
        }
    }

    //TODO return order list api
    private fun callRetuenOrderList() {
        Utils.showLoadingProgress(this@ActMyReturns)
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["user_id"] =
            SharePreference.getStringPref(this@ActMyReturns, SharePreference.userId).toString()
        categoriesProduct["theme_id"]=getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActMyReturns)
                .getReturnOrderList(currentPage.toString(), categoriesProduct)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val orderListResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvMyReturn.show()
                                _binding.tvNoDataFound.hide()
                                currentPage =
                                    orderListResponse?.currentPage!!.toInt()
                                total_pages =
                                    orderListResponse.lastPage!!.toInt()
                                orderListResponse.data?.let {
                                    orderList.addAll(it)
                                }
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvMyReturn.hide()
                                _binding.tvNoDataFound.show()
                            }
                            returnOrderlistAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActMyReturns,
                                orderListResponse?.data?.get(0)?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActMyReturns,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActMyReturns)
                    }else{
                        Utils.errorAlert(
                            this@ActMyReturns,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActMyReturns,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActMyReturns,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        currentPage = 1
        isLastPage = false
        isLoading = false
        orderList.clear()
        callRetuenOrderList()
    }

    //TODO return order data set
    private fun returnOrderListAdapter(returnOrderList: ArrayList<ReturnOrderItem>) {
        _binding.rvMyReturn.layoutManager = manager
        returnOrderlistAdapter =
            ReturnOrderlistAdapter(this@ActMyReturns, returnOrderList) { i: Int, s: String ->
                if (s == Constants.ItemClick) {
                    startActivity(Intent(this@ActMyReturns,ActOrderDetails::class.java).putExtra("order_ID",returnOrderList[i].id.toString()))
                }
            }
        _binding.rvMyReturn.adapter = returnOrderlistAdapter
    }


}