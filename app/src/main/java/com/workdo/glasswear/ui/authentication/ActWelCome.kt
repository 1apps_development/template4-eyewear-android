package com.workdo.glasswear.ui.authentication

import android.content.Intent
import android.view.View
import com.workdo.glasswear.ui.authentication.ActRegisterOption
import com.workdo.glasswear.base.BaseActivity
import com.workdo.glasswear.databinding.ActWelComeBinding
import com.workdo.glasswear.ui.activity.MainActivity

class ActWelCome : BaseActivity() {
    private lateinit var _binding: ActWelComeBinding

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActWelComeBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.btnLogin.setOnClickListener {
            openActivity(ActLoginOption::class.java)
        }
        _binding.btnSignup.setOnClickListener {
            openActivity(ActRegisterOption::class.java)
        }
        _binding.btnGuest.setOnClickListener {
            startActivity(
                Intent(
                    this@ActWelCome,
                    MainActivity::class.java
                )
            )
            finish()
        }
    }

}