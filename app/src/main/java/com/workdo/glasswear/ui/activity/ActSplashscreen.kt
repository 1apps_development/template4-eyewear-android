package com.workdo.glasswear.ui.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Handler
import android.os.Looper
import android.util.Base64
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.workdo.glasswear.R
import com.workdo.glasswear.api.ApiClient
import com.workdo.glasswear.base.BaseActivity
import com.workdo.glasswear.databinding.ActSplashscreenBinding
import com.workdo.glasswear.remote.NetworkResponse
import com.workdo.glasswear.ui.authentication.ActWelCome
import com.workdo.glasswear.utils.SharePreference
import com.workdo.glasswear.utils.Utils
import kotlinx.coroutines.launch
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class ActSplashscreen : BaseActivity() {
    private lateinit var _binding: ActSplashscreenBinding

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActSplashscreenBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        getApiUrl()
        printKeyHash(this@ActSplashscreen)
        Utils.getLog("getShaKey", printKeyHash(this@ActSplashscreen)!!)
        Handler(Looper.getMainLooper()).postDelayed({
            openActivity(MainActivity::class.java)
            finish()
        }, 3000)
    }

    private fun getApiUrl() {
        lifecycleScope.launch {
            val request=HashMap<String,String>()
            request["theme_id"]=resources.getString(R.string.theme_id)
            when (val response = ApiClient.getBaseClient(this@ActSplashscreen).getApiurl(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val apiUrl = response.body
                    when (response.body.status) {
                        1 -> {
                            val responseData=response.body

                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.BaseUrl,
                                responseData.data?.base_url.toString().plus("/"))

                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.PaymentUrl,
                                responseData.data?.payment_url.toString().plus("/"))
                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.ImageUrl,
                                responseData.data?.image_url.toString())
                            getExterUrl()
                        }

                        0 -> {
                            Utils.errorAlert(
                                this@ActSplashscreen,
                                apiUrl.message.toString()
                            )
                        }


                        9 -> {
                            Utils.errorAlert(
                                this@ActSplashscreen,
                                apiUrl.message.toString()
                            )
                            openActivity(ActWelCome::class.java)

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActSplashscreen)
                    } else {
                        Utils.errorAlert(
                            this@ActSplashscreen,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.errorAlert(
                        this@ActSplashscreen,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.errorAlert(
                        this@ActSplashscreen,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun getExterUrl() {
        lifecycleScope.launch {
            var exterURLMap=HashMap<String,String>()
            exterURLMap["theme_id"]=getString(R.string.theme_id)

            when (val response = ApiClient.getClient(this@ActSplashscreen).extraUrl(exterURLMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val extraUrl = response.body
                    when (response.body.status) {
                        1 -> {
                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.Terms,
                                extraUrl.data?.terms.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.Contact_Us,
                                extraUrl.data?.contactUs.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.insta,
                                extraUrl.data?.insta.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.youtube,
                                extraUrl.data?.youtube.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.messanger,
                                extraUrl.data?.messanger.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.twitter,
                                extraUrl.data?.twitter.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.returnPolicy,
                                extraUrl.data?.returnPolicy.toString()
                            )
                        }

                        0 -> {
                            Utils.errorAlert(
                                this@ActSplashscreen,
                                extraUrl.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActSplashscreen,
                                extraUrl.message.toString()
                            )
                            openActivity(ActWelCome::class.java)

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActSplashscreen)
                    } else {
                        Utils.errorAlert(
                            this@ActSplashscreen,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.errorAlert(
                        this@ActSplashscreen,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.errorAlert(
                        this@ActSplashscreen,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    @SuppressLint("PackageManagerGetSignatures")
    fun printKeyHash(context: Activity): String? {
        val packageInfo: PackageInfo
        var key: String? = null
        try {
            val packageName = context.applicationContext.packageName
            packageInfo = context.packageManager.getPackageInfo(
                packageName,
                PackageManager.GET_SIGNATURES
            )
            Log.e("Package Name=", context.applicationContext.packageName)
            for (signature in packageInfo.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                key = String(Base64.encode(md.digest(), 0))
            }
        } catch (e1: PackageManager.NameNotFoundException) {
            Log.e("Name not found", e1.toString())
        } catch (e: NoSuchAlgorithmException) {
            Log.e("No such an algorithm", e.toString())
        } catch (e: Exception) {
            Log.e("Exception", e.toString())
        }
        return key
    }
}