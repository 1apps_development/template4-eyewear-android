package com.workdo.glasswear.ui.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.workdo.glasswear.R
import com.workdo.glasswear.api.ApiClient
import com.workdo.glasswear.base.BaseFragment
import com.workdo.glasswear.databinding.FragSettingBinding
import com.workdo.glasswear.model.EditProfileUpdateModel
import com.workdo.glasswear.model.SingleResponse
import com.workdo.glasswear.remote.NetworkResponse
import com.workdo.glasswear.ui.activity.*
import com.workdo.glasswear.ui.activity.ActChangePassword
import com.workdo.glasswear.ui.authentication.ActWelCome
import com.workdo.glasswear.ui.option.ActMenu
import com.workdo.glasswear.utils.ExtensionFunctions.hide
import com.workdo.glasswear.utils.ExtensionFunctions.show
import com.workdo.glasswear.utils.SharePreference
import com.workdo.glasswear.utils.Utils
import com.workdo.glasswear.utils.Utils.setImageUpload
import com.workdo.glasswear.utils.Utils.setRequestBody
import kotlinx.coroutines.launch
import java.io.File

class FragSetting : BaseFragment<FragSettingBinding>() {

    private lateinit var _binding: FragSettingBinding
    var userId: String = ""
    private var imageFile: File? = null

    override fun initView(view: View) {
        init()
    }

    override fun getBinding(): FragSettingBinding {
        _binding = FragSettingBinding.inflate(layoutInflater)
        return _binding
    }

    override fun onResume() {
        super.onResume()

        if(Utils.isLogin(requireActivity()))
        {
            _binding.tvUserEmail.text =
                SharePreference.getStringPref(requireActivity(), SharePreference.userEmail)
            _binding.tvUserName.text =
                SharePreference.getStringPref(requireActivity(), SharePreference.userFirstName)
                    .plus(" ").plus(
                        SharePreference.getStringPref(
                            requireActivity(),
                            SharePreference.userLastName
                        )
                    )
        }else
        {
            _binding.tvUserName.text=resources.getString(R.string.guest)
            _binding.tvUserEmail.text=resources.getString(R.string.guest_email)
        }
        _binding.tvCount.text = SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)
        if (imageFile == null) {
            Glide.with(requireActivity())
                .load(
                    ApiClient.ImageURL.paymentUrl.plus(
                        SharePreference.getStringPref(
                            requireActivity(),
                            SharePreference.userProfile
                        )
                    )
                ).placeholder(R.drawable.placeholder)
                .into(_binding.ivUserImage)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun init() {
        Log.e("Count", SharePreference.getStringPref(requireActivity(), SharePreference.cartCount).toString())
        userId = SharePreference.getStringPref(requireActivity(), SharePreference.userId).toString()
        if (userId != "") {
            _binding.clMyLogout.show()
            _binding.clMyLogin.hide()

        } else {
            _binding.clMyLogout.hide()
            _binding.clMyLogin.show()
            _binding.tvUserName.text="Guest"
            _binding.tvUserEmail.text="guest@gmail.com"
        }

        _binding.clHistory.setOnClickListener {
            if(Utils.isLogin(requireActivity()))
            {
                openActivity(ActOrderHistory::class.java)

            }else
            {
                openActivity(ActWelCome::class.java)

            }
        }
        _binding.btnEditProfile.setOnClickListener {
            if(Utils.isLogin(requireActivity()))
            {
                openActivity(ActPersonalDetails::class.java)
            }else
            {
                openActivity(ActWelCome::class.java)

            }
        }
        _binding.cleditprofile.setOnClickListener {
            if(Utils.isLogin(requireActivity()))
            {
                openActivity(ActPersonalDetails::class.java)

            }else
            {
                openActivity(ActWelCome::class.java)

            }
        }
        _binding.clPassword.setOnClickListener {
            if(Utils.isLogin(requireActivity()))
            {
                openActivity(ActChangePassword::class.java)

            }else
            {
                openActivity(ActWelCome::class.java)
                requireActivity().finish()
            }
        }
        _binding.clAddress.setOnClickListener {

            if(Utils.isLogin(requireActivity()))
            {
                openActivity(ActGetAddress::class.java)


            }else
            {
                openActivity(ActWelCome::class.java)
                requireActivity().finish()
            }
        }
        _binding.clcart.setOnClickListener {
            openActivity(ActShoppingCart::class.java)
        }
        _binding.ivMenu.setOnClickListener { openActivity(ActMenu::class.java) }
        _binding.clMyLogout.setOnClickListener {
            mLogoutDialog()
        }
        _binding.clMyLogin.setOnClickListener {
            openActivity(ActWelCome::class.java)
            requireActivity().finish()
        }
        _binding.card.setOnClickListener {

            if(Utils.isLogin(requireActivity())) {
                ImagePicker.with(this)
                    .cropSquare()
                    .compress(1024)
                    .saveDir(
                        File(
                            requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                            resources.getString(R.string.app_name)
                        )
                    )
                    .maxResultSize(1080, 1080)
                    .createIntent { intent ->
                        startForProfileImageResult.launch(intent)
                    }
            }else
            {
                openActivity(ActWelCome::class.java)
                requireActivity().finish()
            }
        }
        _binding.clMyReturn.setOnClickListener {
            openActivity(ActMyReturns::class.java)
        }
        _binding.clLoyality.setOnClickListener {
            openActivity(ActLoyality::class.java)
        }
    }

    // Todo OnActivityResult Profile  Picture Update
    private val startForProfileImageResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data
            when (resultCode) {
                Activity.RESULT_OK -> {
                    //Image Uri will not be null for RESULT_OK
                    val fileUri = data?.data!!
                    Log.e("FilePath", fileUri.path.toString())
                    fileUri.path.let { imageFile = File(it) }
                    Log.e("imageFileLength", imageFile!!.length().toString())
                    Glide.with(requireActivity()).load(fileUri.path)
                        .into(_binding.ivUserImage)
                    callSetProfile()
                }
                ImagePicker.RESULT_ERROR -> {
                    Toast.makeText(
                        requireActivity(),
                        ImagePicker.getError(data),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }


    //TODO set profile data
    private fun callSetProfile() {
        Utils.showLoadingProgress(requireActivity())
        var profile=getString(R.string.theme_id)
        var responseData: NetworkResponse<EditProfileUpdateModel, SingleResponse>? = null
        lifecycleScope.launch {
            if (imageFile != null) {
                responseData = ApiClient.getClient(requireActivity()).setUpdateUserImage(
                    setRequestBody(profile),
                    setRequestBody(
                        SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                            .toString()
                    ),
                    setImageUpload("image", imageFile!!)
                )
            }

            when (responseData) {
                is NetworkResponse.Success -> {

                    val response =
                        (responseData as NetworkResponse.Success<EditProfileUpdateModel>).body.data
                    Utils.dismissLoadingProgress()

                    SharePreference.setStringPref(
                        requireActivity(), SharePreference.userProfile,
                        response?.message.toString().toString()
                    )
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if ((responseData as NetworkResponse.ApiError<SingleResponse>).body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    }else{
                        Utils.errorAlert(
                            requireActivity(),
                            (responseData as NetworkResponse.ApiError<SingleResponse>).body.message.toString()
                        )
                    }
                }
                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }
                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.something_went_wrong)
                    )
                }

                else -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.something_went_wrong)
                    )
                }
            }
        }
    }

    //TODO logout api
    private fun callLogout(logout: HashMap<String, String>) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setLogout(logout)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val logout = response.body
                    when (response.body.status) {
                        1 -> {
                            val youtube=SharePreference.getStringPref(requireActivity(), SharePreference.youtube) ?: ""
                            val insta=SharePreference.getStringPref(requireActivity(), SharePreference.insta) ?: ""
                            val messanger=SharePreference.getStringPref(requireActivity(), SharePreference.messanger) ?: ""
                            val twitter=SharePreference.getStringPref(requireActivity(), SharePreference.twitter) ?: ""
                            val returnPolicy=SharePreference.getStringPref(requireActivity(), SharePreference.returnPolicy) ?: ""
                            val Contact_Us=SharePreference.getStringPref(requireActivity(), SharePreference.Contact_Us) ?: ""
                            val Terms=SharePreference.getStringPref(requireActivity(), SharePreference.Terms) ?: ""
                            val baseUrl=SharePreference.getStringPref(requireActivity(), SharePreference.BaseUrl) ?: ""
                            val imageUrl=SharePreference.getStringPref(requireActivity(), SharePreference.ImageUrl) ?: ""
                            val paymentUrl=SharePreference.getStringPref(requireActivity(), SharePreference.PaymentUrl) ?: ""


                            val preference = SharePreference(requireActivity())
                            preference.mLogout()
                            SharePreference.setStringPref(requireActivity(), SharePreference.youtube, youtube)
                            SharePreference.setStringPref(requireActivity(), SharePreference.insta, insta)
                            SharePreference.setStringPref(requireActivity(), SharePreference.messanger, messanger)
                            SharePreference.setStringPref(requireActivity(), SharePreference.twitter, twitter)
                            SharePreference.setStringPref(requireActivity(), SharePreference.returnPolicy, returnPolicy)
                            SharePreference.setStringPref(requireActivity(), SharePreference.Contact_Us, Contact_Us)
                            SharePreference.setStringPref(requireActivity(), SharePreference.Terms, Terms)
                            SharePreference.setStringPref(requireActivity(), SharePreference.BaseUrl, baseUrl)
                            SharePreference.setStringPref(requireActivity(), SharePreference.ImageUrl, imageUrl)
                            SharePreference.setStringPref(requireActivity(), SharePreference.PaymentUrl, paymentUrl)
                            val intent = Intent(activity, ActWelCome::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            activity?.startActivity(intent)
                            activity?.finish()
                        }

                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                logout.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                logout.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    }else{
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun mLogoutDialog() {
        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.logoutdialog)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            dialogInterface.dismiss()
            val logout = HashMap<String, String>()
            logout["user_id"] =
                SharePreference.getStringPref(requireActivity(), SharePreference.userId).toString()
            logout["theme_id"]=getString(R.string.theme_id)
            callLogout(logout)
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            dialogInterface.dismiss()
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }
}