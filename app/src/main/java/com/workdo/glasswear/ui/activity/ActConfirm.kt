package com.workdo.glasswear.ui.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.stripe.android.PaymentConfiguration
import com.stripe.android.paymentsheet.PaymentSheet
import com.stripe.android.paymentsheet.PaymentSheetResult
import com.workdo.glasswear.R
import com.workdo.glasswear.adapter.ConfirmOrderListAdapter
import com.workdo.glasswear.adapter.OrderTaxListAdapter
import com.workdo.glasswear.api.ApiClient
import com.workdo.glasswear.base.BaseActivity
import com.workdo.glasswear.databinding.ActConfirmBinding
import com.workdo.glasswear.model.*
import com.workdo.glasswear.remote.NetworkResponse
import com.workdo.glasswear.ui.authentication.ActWelCome
import com.workdo.glasswear.utils.ExtensionFunctions.hide
import com.workdo.glasswear.utils.ExtensionFunctions.show
import com.workdo.glasswear.utils.SharePreference
import com.workdo.glasswear.utils.Utils
import kotlinx.coroutines.launch
import java.lang.reflect.Type

class ActConfirm : BaseActivity() {
    private lateinit var _binding: ActConfirmBinding
    var billingInfoPost = BillingInfoPost()
    var couponInfoPost = CouponInfoPost()
    var confirmPost = ConfirmModel()
    private var orderDeatils = ArrayList<ProductListItem>()
    private lateinit var orderlistAdapter: ConfirmOrderListAdapter
    private var manager: LinearLayoutManager? = null
    private var taxlist = ArrayList<TaxItem>()
    private lateinit var taxlistAdapter: OrderTaxListAdapter
    private var managerTax: GridLayoutManager? = null
    var currency = ""
    var currencyName = ""
    var paymentType = ""
    var clientSecret = ""
    var total = 0
    var guesttotal = 0
    var stripeKey = ""

    lateinit var paymentSheet: PaymentSheet

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActConfirmBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        stripeKey =
            SharePreference.getStringPref(this@ActConfirm, SharePreference.stripeKey).toString()
        currency =
            SharePreference.getStringPref(this@ActConfirm, SharePreference.currency).toString()
        currencyName =
            SharePreference.getStringPref(this@ActConfirm, SharePreference.currency_name)
                .toString()
        paymentType = SharePreference.getStringPref(this@ActConfirm, SharePreference.Payment_Type)
            .toString()
        manager = LinearLayoutManager(this@ActConfirm)
        managerTax = GridLayoutManager(this@ActConfirm, 1, GridLayoutManager.HORIZONTAL, false)
        _binding.ivBack.setOnClickListener { finish() }

        if (Utils.isLogin(this@ActConfirm)) {
            confirmPost.userId =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.userId).toString()
                    .toInt()
            confirmPost.paymentType =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Payment_Type)
                    .toString()
            confirmPost.paymentComment =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Payment_Comment)
                    .toString()
            confirmPost.themeId=resources.getString(R.string.theme_id)
            confirmPost.deliveryId =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Delivery_Id)?.toInt()
            confirmPost.deliveryComment =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Delivery_Comment)
                    .toString()
            couponInfoPost.couponId =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Coupon_Id).toString()
            couponInfoPost.couponName =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Coupon_Name)
                    .toString()
            couponInfoPost.couponCode =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Coupon_Code)
                    .toString()
            couponInfoPost.couponDiscountType =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Coupon_Discount_Type)
                    .toString()
            couponInfoPost.couponDiscountAmount =
                SharePreference.getStringPref(
                    this@ActConfirm,
                    SharePreference.Coupon_Discount_Amount
                )
                    .toString()
            couponInfoPost.couponFinalAmount =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Coupon_Final_Amount)
                    .toString()
            couponInfoPost.couponDiscountNumber =
                SharePreference.getStringPref(
                    this@ActConfirm,
                    SharePreference.Coupon_Discount_Number
                )
                    .toString()
            billingInfoPost.firstname =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.userFirstName)
                    .toString()
            billingInfoPost.lastname =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.userLastName)
                    .toString()
            billingInfoPost.email =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.userEmail).toString()
            billingInfoPost.billingUserTelephone =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.userMobile)
                    .toString()
            billingInfoPost.billingCompanyName =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Billing_Company_Name)
                    .toString()
            billingInfoPost.billingAddress =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Billing_Address)
                    .toString()
            billingInfoPost.billingPostecode =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Billing_Postecode)
                    .toString()
            billingInfoPost.billingCountry =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Billing_Country)
                    .toString().toInt()
            billingInfoPost.billingState =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Billing_State)
                    .toString()
                    .toInt()
            billingInfoPost.billingCity =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Billing_City)
                    .toString()
            billingInfoPost.deliveryAddress =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Delivery_Address)
                    .toString()
            billingInfoPost.deliveryPostcode =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Delivery_Postcode)
                    .toString()
            billingInfoPost.deliveryCountry =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Delivery_Country)
                    .toString().toInt()
            billingInfoPost.deliveryState =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Delivery_State)
                    .toString().toInt()
            billingInfoPost.deliveryCity =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Delivery_City)
                    .toString()
            val coupon_id =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Coupon_Id).toString()
            if (coupon_id == "") {
                confirmPost.couponInfo = CouponInfoPost()
            } else {
                confirmPost.couponInfo = couponInfoPost
            }
            confirmPost.billingInfo = billingInfoPost
            orderDeatilsAdapter(orderDeatils)
            taxAdapter(taxlist)

            confirmOrderApi()
            _binding.btnCheckout.setOnClickListener {
                if (paymentType == "stripe") {
                    paymentFlow()
                } else {
                    placeOrder()
                }
            }
        } else {

            _binding.emptyview.hide()
            val productType: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type
            val type: Type = object : TypeToken<ArrayList<TaxItem>>() {}.type
            val productList =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.GuestCartList)
            val taxList = SharePreference.getStringPref(this@ActConfirm, SharePreference.TaxInfo)
            val billingDetails =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.BillingDetails)
            val guestCouponData =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.GuestCouponData)
            var cartList = Gson().fromJson<ArrayList<ProductListItem>>(productList, productType)
            var taxInfoList = Gson().fromJson<ArrayList<TaxItem>>(taxList, type)

            val billingDetailsData =
                Gson().fromJson(billingDetails, BillingDetailsAddress::class.java)
            orderDeatilsAdapter(cartList)
            taxAdapter(taxInfoList)

            setGuestLoginData(billingDetailsData)

            val paymentImageUrl =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.PaymentImage)
            val deliveryImageUrl =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.DeliveryImage)
            setpayment(paymentImageUrl, deliveryImageUrl)

            _binding.tvTotalPrice.text =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.GuestCartTotal)
            _binding.tvSub.text =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.GuestCartSubTotal)

            val price = SharePreference.getStringPref(this@ActConfirm, SharePreference.FinalPrice).toString().toDouble()
            Log.e("FinalPrice",price.toString())
            total = (price * 100).toInt()
            Log.e("FinalPrice",guesttotal.toString())


            paymentSheetApi()
            var couponData = Gson().fromJson(guestCouponData, CouponData::class.java) ?: null
            Log.e("Coupondata", Gson().toJson(couponData))

            val couponInfo: CouponInfoPost = if (couponData == null) {
                CouponInfoPost()
            } else {
                CouponInfoPost(
                    couponData.couponDiscountType,
                    couponData.code.toString(),
                    couponData.id.toString(),
                    couponData.couponDiscountAmount.toString(),
                    couponData.amount.toString(),
                    couponData.finalPrice,
                    couponData.name.toString()
                )
            }

            Log.e("data", Gson().toJson(couponInfo))
            setCoupon(couponData)

            val productDataList = ArrayList<Product>()
            for (i in 0 until cartList.size) {
                productDataList.add(
                    Product(
                        cartList[i].productId.toString(),
                        cartList[i].qty.toString(),
                        cartList[i].variantId.toString()
                    )
                )
            }

            _binding.btnCheckout.setOnClickListener {
                val request = GuestRequest()
                request.billingData = billingDetailsData

                request.couponData = couponInfo
                request.product = productDataList
                request.taxItem = taxInfoList
                request.paymentType =
                    SharePreference.getStringPref(this@ActConfirm, SharePreference.Payment_Type)
                request.themeId=resources.getString(R.string.theme_id)
                request.paymentComment =
                    SharePreference.getStringPref(this@ActConfirm, SharePreference.Payment_Comment)
                request.deliveryId =
                    SharePreference.getStringPref(this@ActConfirm, SharePreference.Delivery_Id)
                request.deliveryComment =
                    SharePreference.getStringPref(this@ActConfirm, SharePreference.Delivery_Comment)
                if (paymentType == "stripe") {

                    paymentFlow()
                } else {
                    guestPlaceOrder(request)
                }
            }
        }

        Log.e(
            "UserId",
            Utils.getStringPreference(this@ActConfirm, SharePreference.userId).toString()
        )
        PaymentConfiguration.init(
            this@ActConfirm,
            stripeKey
        )
        paymentSheet = PaymentSheet(this@ActConfirm, ::onPaymentSheetResult)
    }


    private fun paymentFlow() {
        paymentSheet.presentWithPaymentIntent(
            clientSecret,
            PaymentSheet.Configuration(
                getString(R.string.app_name)
            )
        )
    }

    protected open fun onPaymentSheetResult(
        paymentResult: PaymentSheetResult
    ) {
        if (paymentResult is PaymentSheetResult.Completed) {
            orderApiCalling()
        }
    }

    private fun orderApiCalling() {
        if (Utils.isLogin(this@ActConfirm)) {
            /* confirmPost.userId =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.userId)
                     .toString()
                     .toInt()
             confirmPost.paymentType =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.Payment_Type)
                     .toString()
             confirmPost.paymentComment =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.Payment_Comment)
                     .toString()
             confirmPost.deliveryId =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.Delivery_Id)
                     ?.toInt()
             confirmPost.deliveryComment =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.Delivery_Comment)
                     .toString()
             couponInfoPost.couponId =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.Coupon_Id)
                     .toString()
             couponInfoPost.couponName =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.Coupon_Name)
                     .toString()
             couponInfoPost.couponCode =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.Coupon_Code)
                     .toString()
             couponInfoPost.couponDiscountType =
                 SharePreference.getStringPref(
                     this@ActConfirm,
                     SharePreference.Coupon_Discount_Type
                 )
                     .toString()
             couponInfoPost.couponDiscountAmount =
                 SharePreference.getStringPref(
                     this@ActConfirm,
                     SharePreference.Coupon_Discount_Amount
                 )
                     .toString()
             couponInfoPost.couponFinalAmount =
                 SharePreference.getStringPref(
                     this@ActConfirm,
                     SharePreference.Coupon_Final_Amount
                 )
                     .toString()
             couponInfoPost.couponDiscountNumber =
                 SharePreference.getStringPref(
                     this@ActConfirm,
                     SharePreference.Coupon_Discount_Number
                 )
                     .toString()
             billingInfoPost.firstname =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.userFirstName)
                     .toString()
             billingInfoPost.lastname =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.userLastName)
                     .toString()
             billingInfoPost.email =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.userEmail)
                     .toString()
             billingInfoPost.billingUserTelephone =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.userMobile)
                     .toString()
             billingInfoPost.billingCompanyName =
                 SharePreference.getStringPref(
                     this@ActConfirm,
                     SharePreference.Billing_Company_Name
                 )
                     .toString()
             billingInfoPost.billingAddress =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.Billing_Address)
                     .toString()
             billingInfoPost.billingPostecode =
                 SharePreference.getStringPref(
                     this@ActConfirm,
                     SharePreference.Billing_Postecode
                 )
                     .toString()
             billingInfoPost.billingCountry =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.Billing_Country)
                     .toString().toInt()
             billingInfoPost.billingState =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.Billing_State)
                     .toString()
                     .toInt()
             billingInfoPost.billingCity =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.Billing_City)
                     .toString()
             billingInfoPost.deliveryAddress =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.Delivery_Address)
                     .toString()
             billingInfoPost.deliveryPostcode =
                 SharePreference.getStringPref(
                     this@ActConfirm,
                     SharePreference.Delivery_Postcode
                 )
                     .toString()
             billingInfoPost.deliveryCountry =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.Delivery_Country)
                     .toString().toInt()
             billingInfoPost.deliveryState =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.Delivery_State)
                     .toString().toInt()
             billingInfoPost.deliveryCity =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.Delivery_City)
                     .toString()
             val coupon_id =
                 SharePreference.getStringPref(this@ActConfirm, SharePreference.Coupon_Id)
                     .toString()
             if (coupon_id == "") {
                 confirmPost.couponInfo = CouponInfoPost()
             } else {
                 confirmPost.couponInfo = couponInfoPost
             }
             confirmPost.billingInfo = billingInfoPost*/

            placeOrder()

        } else {

            val productType: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type
            val type: Type = object : TypeToken<ArrayList<TaxItem>>() {}.type
            val productList =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.GuestCartList)
            val taxList =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.TaxInfo)
            val billingDetails =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.BillingDetails)
            val guestCouponData =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.GuestCouponData)
            var cartList = Gson().fromJson<ArrayList<ProductListItem>>(productList, productType)
            var taxInfoList = Gson().fromJson<ArrayList<TaxItem>>(taxList, type)

            val billingDetailsData =
                Gson().fromJson(billingDetails, BillingDetailsAddress::class.java)


            var couponData = Gson().fromJson(guestCouponData, CouponData::class.java) ?: null
            Log.e("Coupondata", Gson().toJson(couponData))

            val couponInfo: CouponInfoPost = if (couponData == null) {
                CouponInfoPost()
            } else {
                CouponInfoPost(
                    couponData.couponDiscountType,
                    couponData.code.toString(),
                    couponData.id.toString(),
                    couponData.couponDiscountAmount.toString(),
                    couponData.amount.toString(),
                    couponData.finalPrice,
                    couponData.name.toString()
                )
            }

            Log.e("data", Gson().toJson(couponInfo))

            val productDataList = ArrayList<Product>()
            for (i in 0 until cartList.size) {
                productDataList.add(
                    Product(
                        cartList[i].productId.toString(),
                        cartList[i].qty.toString(),
                        cartList[i].variantId.toString()
                    )
                )
            }


            val request = GuestRequest()
            request.billingData = billingDetailsData

            request.couponData = couponInfo
            request.product = productDataList
            request.taxItem = taxInfoList
            request.paymentType =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Payment_Type)
            request.paymentComment =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Payment_Comment)
            request.deliveryId =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Delivery_Id)
            request.deliveryComment =
                SharePreference.getStringPref(this@ActConfirm, SharePreference.Delivery_Comment)

            guestPlaceOrder(request)

        }
    }


    private fun paymentSheetApi() {
        Utils.showLoadingProgress(this@ActConfirm)
        val request = HashMap<String, String>()
        request["price"] = total
            .toString()
        request["currency"] =
            SharePreference.getStringPref(this@ActConfirm, SharePreference.currency_name).toString()
        request["theme_id"]=resources.getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClientPayment(this@ActConfirm)
                .paymentSheet(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    clientSecret = response.body.clientSecret.toString()
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActConfirm)
                    } else {
                        Utils.errorAlert(
                            this@ActConfirm,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActConfirm,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActConfirm, "Something went wrong")
                }
            }
        }
    }


    private fun placeOrder() {
        Utils.showLoadingProgress(this@ActConfirm)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActConfirm)
                .placeOrder(confirmPost)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val placeOrderResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            startActivity(
                                Intent(
                                    this@ActConfirm,
                                    ActOrderSuccessfully::class.java
                                ).putExtra(
                                    "orderTitle",
                                    placeOrderResponse?.completeOrder?.orderComplate?.orderComplateTitle
                                ).putExtra(
                                    "desc",
                                    placeOrderResponse?.completeOrder?.orderComplate?.orderComplateDescription
                                )
                            )
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActConfirm,
                                placeOrderResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActConfirm,
                                placeOrderResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActConfirm)
                    } else {
                        Utils.errorAlert(
                            this@ActConfirm,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActConfirm,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActConfirm, "Something went wrong")
                }
            }
        }
    }


    private fun guestPlaceOrder(requestData: GuestRequest) {

        Utils.showLoadingProgress(this@ActConfirm)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActConfirm)
                .guestPlaceOrder(requestData)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val placeOrderResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            startActivity(
                                Intent(
                                    this@ActConfirm,
                                    ActOrderSuccessfully::class.java
                                ).putExtra(
                                    "orderTitle",
                                    placeOrderResponse?.completeOrder?.orderComplate?.orderComplateTitle
                                ).putExtra(
                                    "desc",
                                    placeOrderResponse?.completeOrder?.orderComplate?.orderComplateDescription
                                )
                            )
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActConfirm,
                                placeOrderResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActConfirm,
                                placeOrderResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActConfirm)
                    } else {
                        Utils.errorAlert(
                            this@ActConfirm,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActConfirm,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActConfirm, "Something went wrong")
                }
            }
        }
    }


    private fun confirmOrderApi() {
        Utils.showLoadingProgress(this@ActConfirm)
        lifecycleScope.launch {

            when (val response = ApiClient.getClient(this@ActConfirm)
                .confirmOrder(confirmPost)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val confirmResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.product?.size ?: 0) > 0) {
                                _binding.rvOrderDetails.show()
                                _binding.emptyview.hide()
                                confirmResponse?.product?.let {
                                    orderDeatils.addAll(it)
                                }
                                confirmResponse?.tax?.let {
                                    taxlist.addAll(it)
                                }
                                val price = confirmResponse?.finalPrice.toString().toDouble()
                                total = (price * 100).toInt()
                                paymentSheetApi()

                            } else {
                                _binding.rvOrderDetails.hide()
                                _binding.emptyview.show()

                            }
                            orderlistAdapter.notifyDataSetChanged()
                            taxlistAdapter.notifyDataSetChanged()
                            _binding.tvTotalPrice.text =
                                currency.plus(Utils.getPrice(confirmResponse?.finalPrice.toString()))
                            _binding.tvSub.text =
                                currency.plus(Utils.getPrice(confirmResponse?.subtotal.toString()))

                            setCoupon(confirmResponse?.couponInfo)
                            setBillInfo(
                                confirmResponse?.billingInformation,
                                confirmResponse?.deliveryInformation
                            )
                            setpayment(
                                confirmResponse?.paymnet,
                                confirmResponse?.delivery
                            )
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActConfirm,
                                confirmResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActConfirm,
                                confirmResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActConfirm)
                    } else {
                        Utils.errorAlert(
                            this@ActConfirm,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActConfirm,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActConfirm, "Something went wrong")
                }
            }
        }
    }

    private fun setCoupon(couponInfo: CouponData?) {
        if (couponInfo == null || Gson().toJson(couponInfo) == "{}") {
            _binding.tvCouponCode.hide()
            _binding.tvDescountDesc.hide()
            _binding.tvCouponPrice.hide()
            _binding.tvCouponCodeName.hide()
            _binding.view2.hide()
        } else {
            _binding.tvCouponCode.show()
            _binding.view2.show()
            _binding.tvDescountDesc.show()
            _binding.tvCouponPrice.show()
            _binding.tvCouponCodeName.show()
            if (Utils.isLogin(this@ActConfirm)) {
                _binding.tvDescountDesc.text = couponInfo.discountString
                _binding.tvCouponPrice.text = couponInfo.discountString2.toString()
            } else {
                _binding.tvDescountDesc.text =
                    "-${couponInfo.amount.plus(currency)} for all products"
                _binding.tvCouponPrice.text = "(-${couponInfo.amount.plus(currencyName)})"
            }
            _binding.tvCouponCodeName.text = couponInfo.code
        }
    }

    private fun setpayment(paymnet: String?, delivery: String?) {
        Glide.with(this@ActConfirm)
            .load(ApiClient.ImageURL.paymentUrl.plus(paymnet)).into(_binding.ivPaymentType)
        Glide.with(this@ActConfirm)
            .load(ApiClient.ImageURL.BASE_URL.plus(delivery)).into(_binding.ivDeliveryType)
    }

    private fun setBillInfo(
        billingInformations: BillingInformation?,
        deliveryInformations: DeliveryInformation?
    ) {
        _binding.tvBillUserName.text = billingInformations?.name
        _binding.tvBillUserAddress.text =
            billingInformations?.address.plus(",").plus(" \n").plus(billingInformations?.city)
                .plus(", ")
                .plus(" \n")
                .plus(billingInformations?.state).plus(",")
                .plus("\n").plus(billingInformations?.country).plus(" - ")
                .plus(billingInformations?.postecode).plus(".")
        _binding.tvBillUserPhone.text = getString(R.string.phone_).plus(billingInformations?.phone)
        _binding.tvBillUserEmail.text = getString(R.string.email_).plus(billingInformations?.email)

        _binding.tvDeliveryUserName.text = deliveryInformations?.name
        _binding.tvDeliveryUserAddress.text =
            deliveryInformations?.address.plus(",").plus(" \n").plus(deliveryInformations?.city)
                .plus(",").plus(" ")
                .plus(" \n")
                .plus(deliveryInformations?.state).plus(",")
                .plus("\n").plus(deliveryInformations?.country).plus(" - ")
                .plus(deliveryInformations?.postecode).plus(".")
        _binding.tvDeliveryUserPhone.text =
            getString(R.string.phone_).plus(deliveryInformations?.phone)
        _binding.tvDeliveryUserEmail.text =
            getString(R.string.email_).plus(deliveryInformations?.email)
    }


    private fun setGuestLoginData(billingData: BillingDetailsAddress) {
        _binding.tvBillUserName.text =
            billingData?.firstname.toString().plus(" ${billingData.lastname}")
        _binding.tvBillUserAddress.text =
            billingData?.billing_address.plus(",").plus(" \n").plus(billingData?.billing_city)
                .plus(", ").plus(" \n")
                .plus(billingData.billingStateName).plus(",")
                .plus("\n").plus(billingData.billingCountryName).plus(" - ")
                .plus(billingData?.billing_postecode).plus(".")
        _binding.tvBillUserPhone.text =
            getString(R.string.phone_).plus(billingData.billing_user_telephone)
        _binding.tvBillUserEmail.text = getString(R.string.email_).plus(billingData.email)

        _binding.tvDeliveryUserName.text =
            billingData?.firstname.toString().plus(" ${billingData.lastname}")
        _binding.tvDeliveryUserAddress.text =
            billingData?.delivery_address.plus(",").plus(" \n").plus(billingData?.delivery_city)
                .plus(",").plus(" \n")

                .plus(billingData?.deliveryStateName).plus(",")
                .plus("\n").plus(billingData?.deliveryCountryName).plus(" - ")
                .plus(billingData?.delivery_postcode).plus(".")
        _binding.tvDeliveryUserPhone.text =
            getString(R.string.phone_).plus(billingData?.billing_user_telephone)
        _binding.tvDeliveryUserEmail.text =
            getString(R.string.email_).plus(billingData?.email)
    }

    private fun orderDeatilsAdapter(orderDeatils: ArrayList<ProductListItem>) {
        _binding.rvOrderDetails.layoutManager = manager
        orderlistAdapter =
            ConfirmOrderListAdapter(this@ActConfirm, orderDeatils) { i: Int, s: String ->
            }


        _binding.rvOrderDetails.adapter = orderlistAdapter
    }

    private fun taxAdapter(taxList: ArrayList<TaxItem>) {
        _binding.rvTax.layoutManager = managerTax
        taxlistAdapter =
            OrderTaxListAdapter(this@ActConfirm, taxList) { i: Int, s: String ->
            }
        _binding.rvTax.adapter = taxlistAdapter
    }


}