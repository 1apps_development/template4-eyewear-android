package com.workdo.glasswear.ui.option

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.workdo.glasswear.R
import com.workdo.glasswear.adapter.SearchListAdapter
import com.workdo.glasswear.api.ApiClient
import com.workdo.glasswear.base.BaseActivity
import com.workdo.glasswear.databinding.ActSearchBinding
import com.workdo.glasswear.databinding.DlgConfirmBinding
import com.workdo.glasswear.model.FeaturedProductsSub
import com.workdo.glasswear.model.ProductListItem
import com.workdo.glasswear.remote.NetworkResponse
import com.workdo.glasswear.ui.activity.ActProductDetails
import com.workdo.glasswear.ui.activity.ActShoppingCart
import com.workdo.glasswear.ui.authentication.ActWelCome
import com.workdo.glasswear.utils.Constants
import com.workdo.glasswear.utils.ExtensionFunctions.hide
import com.workdo.glasswear.utils.ExtensionFunctions.hideKeyboard
import com.workdo.glasswear.utils.ExtensionFunctions.show
import com.workdo.glasswear.utils.PaginationScrollListener
import com.workdo.glasswear.utils.SharePreference
import com.workdo.glasswear.utils.Utils
import kotlinx.coroutines.launch
import java.lang.reflect.Type
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ActSearch : BaseActivity() {
    private lateinit var _binding: ActSearchBinding
    private var managersearch: GridLayoutManager? = null
    private var searchList = ArrayList<FeaturedProductsSub>()
    private lateinit var searchAdapter: SearchListAdapter
    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0
    var ratting = ""
    var name: String = ""
    var id: String = ""
    var minValue: String = ""
    var maxValue: String = ""
    var filterType: String = ""
    var count = 1
    var text: String = ""

    override fun setLayout(): View = _binding.root
    override fun initView() {
        _binding = ActSearchBinding.inflate(layoutInflater)
        init()

    }

    private fun init() {
        managersearch =
            GridLayoutManager(this@ActSearch, 2, GridLayoutManager.VERTICAL, false)
        paginationBestSeller()
        bestsellerAdapter(searchList)
        if (filterType == "filter") {
            _binding.edSearch.text.clear()
        }
        _binding.ivFilter.setOnClickListener {
            val intent = Intent(this@ActSearch, ActFilter::class.java)
            resultLauncher.launch(intent)
        }
        _binding.ivBack.setOnClickListener { finish() }
        _binding.edSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                hideKeyboard()
                text = _binding.edSearch.text.toString()
                    .lowercase(Locale.getDefault())
                Log.e("SerachText", text)
                currentPage = 1
                isLastPage = false
                isLoading = false
                searchList.clear()
                if (filterType == "") {
                    val searchmap = HashMap<String, String>()
                    searchmap["type"] = "product_search"
                    searchmap["name"] = text
                    searchmap["theme_id"] = getString(R.string.theme_id)
                    callsearchApi(searchmap)
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }
        })
    }


    private fun paginationBestSeller() {
        val paginationListener = object : PaginationScrollListener(managersearch) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                if (filterType == "filter") {
                    val searchmap = HashMap<String, String>()
                    searchmap["type"] = "product_filter"
                    searchmap["name"] = text
                    searchmap["tag"] = id
                    searchmap["min_price"] = minValue
                    searchmap["max_price"] = maxValue
                    searchmap["rating"] = ratting
                    searchmap["theme_id"] = getString(R.string.theme_id)
                    callsearchApi(searchmap)
                } else {
                    val searchmap = HashMap<String, String>()
                    searchmap["type"] = "product_search"
                    searchmap["name"] = text
                    searchmap["theme_id"] = getString(R.string.theme_id)
                    callsearchApi(searchmap)
                }
            }
        }
        _binding.rvSearch.addOnScrollListener(paginationListener)
    }

    //TODO Adapter set Best seller
    private fun bestsellerAdapter(bestsellersList: ArrayList<FeaturedProductsSub>) {
        _binding.rvSearch.layoutManager = managersearch
        searchAdapter =
            SearchListAdapter(this@ActSearch, bestsellersList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            this@ActSearch,
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (bestsellersList[i].inWhishlist == false) {
                            callWishlist(
                                "add", bestsellersList[i].id, "BestsellersList", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                bestsellersList[i].id,
                                "BestsellersList",
                                i
                            )
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        finish()
                    }
                } else if (s == Constants.CartClick) {
                    if (!Utils.isLogin(this@ActSearch)) {
                        guestUserAddToCart(bestsellersList[i], bestsellersList[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(
                                this@ActSearch,
                                SharePreference.userId
                            ).toString()
                        addtocart["product_id"] = bestsellersList[i].id.toString()
                        addtocart["variant_id"] = bestsellersList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"]=getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            bestsellersList[i].id,
                            bestsellersList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(this@ActSearch, ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, bestsellersList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvSearch.adapter = searchAdapter
    }

    //TODO Guest user add to cart
    private fun guestUserAddToCart(data: FeaturedProductsSub, id: Int?) {
        val cartData = cartData(data)
        Log.e("cartData2", Gson().toJson(cartData))
        val cartListData = SharePreference.getStringPref(
            this@ActSearch,
            SharePreference.GuestCartList
        )
        val type: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type
        var cartList = Gson().fromJson<ArrayList<ProductListItem>>(cartListData, type)
        Log.e("cartListBefore2", Gson().toJson(cartList))
        if (cartList == null) {
            val cartDataList = ArrayList<ProductListItem>()
            cartDataList.add(cartData)
            SharePreference.setStringPref(
                this@ActSearch,
                SharePreference.GuestCartList,
                Gson().toJson(cartDataList)
            )
            SharePreference.setStringPref(
                this@ActSearch,
                SharePreference.cartCount,
                cartDataList.size.toString()
            )
        } else if (isProductAlreadyAdded(cartList, data.id.toString())) {
            //Utils.errorAlert(this@ActSearch, "item already added into cart")
            for (i in 0 until cartList.size) {
                if (cartList[i].productId?.toInt() == id) {
                    val position = cartList[i]
                    Log.e("SelectId", position.toString())
                    dlgAlreadyCart(data.message, data.id, data.defaultVariantId, i)
                }
            }

        } else {
            dlgConfirm(data.name.plus(" ").plus("add successfully."))
            cartList.add(cartData)
            SharePreference.setStringPref(
                this@ActSearch,
                SharePreference.GuestCartList,
                Gson().toJson(cartList)
            )
            SharePreference.setStringPref(
                this@ActSearch,
                SharePreference.cartCount,
                cartList.size.toString()
            )
        }
    }

    //TODO cart data
    private fun cartData(data: FeaturedProductsSub): ProductListItem {
        return ProductListItem(
            data.coverImagePath.toString(),
            data.defaultVariantId.toString(),
            data.finalPrice.toString(),
            data.discountPrice.toString(),
            data.id.toString(),
            1,
            data.name.toString(),
            "",
            "",
            data.variantName.toString(),
            data.originalPrice.toString()
        )
    }

    private fun isProductAlreadyAdded(cartList: ArrayList<ProductListItem>, id: String): Boolean {
        return cartList.filter { it.productId == id.toString() }.size == 1
    }

    //TODO wishlist api
    private fun callWishlist(type: String, id: Int?, itemType: String, position: Int) {
        Utils.showLoadingProgress(this@ActSearch)
        val wishlist = HashMap<String, String>()
        wishlist["user_id"] =
            SharePreference.getStringPref(this@ActSearch, SharePreference.userId).toString()
        wishlist["product_id"] = id.toString()
        wishlist["wishlist_type"] = type
        wishlist["theme_id"]=getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActSearch)
                .setWishlist(wishlist)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if (type == "add") {
                                if (itemType == "BestsellersList") {
                                    searchList[position].inWhishlist = true
                                    searchAdapter.notifyItemChanged(position)
                                }
                            } else {
                                if (itemType == "BestsellersList") {
                                    searchList[position].inWhishlist = false
                                    searchAdapter.notifyItemChanged(position)
                                }
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActSearch,
                                wishlistResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActSearch,
                                wishlistResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            this@ActSearch.finish()
                            this@ActSearch.finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActSearch)
                    } else {
                        Utils.errorAlert(
                            this@ActSearch,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActSearch,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActSearch,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO add to cart api
    private fun addtocartApi(
        addtocart: HashMap<String, String>,
        id: Int?,
        defaultVariantId: Int?,
        i: Int?
    ) {
        Utils.showLoadingProgress(this@ActSearch)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActSearch)
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtocart = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            SharePreference.setStringPref(
                                this@ActSearch,
                                SharePreference.cartCount,
                                addtocart?.count.toString()
                            )
                        }
                        0 -> {
                            dlgAlreadyCart(response.body.data?.message, id, defaultVariantId, i)
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActSearch,
                                addtocart?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            this@ActSearch.finish()
                            this@ActSearch.finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActSearch)
                    } else {
                        Utils.errorAlert(
                            this@ActSearch,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActSearch,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActSearch,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO checkout or Continue shopping dialog
    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(this@ActSearch)
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setOnClickListener {
            dialog.dismiss()
            openActivity(ActShoppingCart::class.java)
        }
        confirmDialogBinding.tvContinueShopping.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    //TODO Item already cart dialog
    private fun dlgAlreadyCart(message: String?, id: Int?, defaultVariantId: Int?, i: Int?) {
        val builder = AlertDialog.Builder(this@ActSearch)
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.product_already_again)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val cartqty = java.util.HashMap<String, String>()

            if (Utils.isLogin(this@ActSearch)) {
                count = 1
                cartqty["product_id"] = id.toString()
                cartqty["user_id"] =
                    SharePreference.getStringPref(
                        this@ActSearch,
                        SharePreference.userId
                    ).toString()
                cartqty["variant_id"] = defaultVariantId.toString()
                cartqty["quantity_type"] = "increase"
                cartqty["theme_id"]=getString(R.string.theme_id)
                cartQtyApi(cartqty, "increase")
            } else {
                val cartListData = SharePreference.getStringPref(
                    this@ActSearch,
                    SharePreference.GuestCartList
                )
                val type: Type = object : TypeToken<java.util.ArrayList<ProductListItem>>() {}.type
                var cartList =
                    Gson().fromJson<java.util.ArrayList<ProductListItem>>(cartListData, type)
                var count = cartList[i!!].qty?.toInt()!!
                Log.e("Count", count.toString())
                cartList[i!!].qty = count.plus(1)

                manageOfflineData(cartList)
            }
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    //TODO Cart qty api
    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>,
        type: String
    ) {
        Utils.showLoadingProgress(this@ActSearch)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActSearch)
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        this@ActSearch,
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {
                            /* when (type) {
                                 "increase" -> {
                                     cartList[position].qty = count
                                     val totalQty = cartList.sumOf { it.qty!! }
                                     _binding.tvTotalQtyCount.text = totalQty.toString()
                                     cartListAdapter.notifyDataSetChanged()

                                 }
                                 "remove" -> {
                                     cartList.removeAt(position)
                                     cartListAdapter.notifyDataSetChanged()
                                     if (cartList.size > 0) {
                                         _binding.rvShoppingCart.show()
                                         _binding.tvTotalQtyCount.show()
                                         _binding.clCoupon.show()
                                         _binding.tvNoDataFound.hide()
                                         _binding.clPaymentDetails.show()
                                     } else {
                                         _binding.rvShoppingCart.hide()
                                         _binding.tvTotalQtyCount.hide()
                                         _binding.clCoupon.hide()
                                         _binding.tvNoDataFound.show()
                                         _binding.clPaymentDetails.hide()
                                     }
                                     val totalQty = cartList.sumOf { it.qty!! }
                                     _binding.tvTotalQtyCount.text = totalQty.toString()
                                 }
                                 "decrease" -> {
                                     cartList[position].qty = count
                                     val totalQty = cartList.sumOf { it.qty!! }
                                     _binding.tvTotalQtyCount.text = totalQty.toString()
                                     cartListAdapter.notifyDataSetChanged()
                                 }
                             }
                             getCartList()*/
                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActSearch,
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActSearch,
                                cartQtyResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActSearch)
                    } else {
                        Utils.errorAlert(
                            this@ActSearch, response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActSearch,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActSearch, "Something went wrong")
                }
            }
        }
    }

    //TODO guest user offline data
    private fun manageOfflineData(cartItemList: java.util.ArrayList<ProductListItem>) {
        SharePreference.setStringPref(
            this@ActSearch,
            SharePreference.GuestCartList,
            Gson().toJson(cartItemList)
        )
        SharePreference.setStringPref(
            this@ActSearch,
            SharePreference.cartCount,
            cartItemList.size.toString()
        )
    }

    //TODO seach api
    private fun callsearchApi(searchmap: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActSearch)
        lifecycleScope.launch {

            val response = if (SharePreference.getStringPref(this@ActSearch, SharePreference.token)
                    ?.isEmpty() == true)
            {
                ApiClient.getClient(this@ActSearch).searchListGuests(currentPage.toString(), searchmap)
            } else {
                ApiClient.getClient(this@ActSearch).searchList(currentPage.toString(), searchmap)
            }


            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val searchResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            // searchList.clear()
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvSearch.show()
                                _binding.tvResultsFound.show()
                                currentPage = searchResponse?.currentPage!!.toInt()
                                total_pages = searchResponse.lastPage!!.toInt()
                                searchResponse.data?.let {
                                    searchList.addAll(it)
                                }
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false

                            } else {
                                _binding.rvSearch.hide()
                                _binding.tvResultsFound.hide()
                            }
                            searchAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            _binding.rvSearch.hide()
                            _binding.tvResultsFound.hide()

                            Utils.errorAlert(
                                this@ActSearch,
                                searchResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActSearch,
                                searchResponse?.data?.get(0)?.message.toString()
                            )
                            Utils.setInvalidToekn(this@ActSearch)
                        }
                    }
                    if (searchResponse?.to == null) {
                        _binding.tvResultsFound.text =
                            "0".plus(" ").plus(getString(R.string.result_fund))
                    } else {
                        _binding.tvResultsFound.text =
                            searchResponse.total.toString().plus(" ")
                                .plus(getString(R.string.result_fund))
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActSearch)
                    } else {
                        Utils.errorAlert(
                            this@ActSearch,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActSearch,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActSearch,
                        getString(R.string.something_went_wrong)
                    )
                }
            }
        }
    }

    //TODO On Activity Intent Result
    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == 500) {
                ratting = result.data?.getStringExtra("ratting").toString()
                name = result.data?.getStringExtra("name").toString()
                id = result.data?.getStringExtra("id").toString()
                minValue = result.data?.getStringExtra("minValue").toString()
                maxValue = result.data?.getStringExtra("maxValue").toString()
                filterType = result.data?.getStringExtra("filter").toString()
                Log.e("SearchData", id + minValue + maxValue + ratting)
                isLoading = false
                isLastPage = false
                currentPage = 1
                total_pages = 0
                searchList.clear()

                val searchmap = HashMap<String, String>()
                searchmap["type"] = "product_filter"
                searchmap["name"] = text
                searchmap["tag"] = result.data?.getStringExtra("id").toString()
                searchmap["min_price"] = result.data?.getStringExtra("minValue").toString()
                searchmap["max_price"] = result.data?.getStringExtra("maxValue").toString()
                searchmap["rating"] = result.data?.getStringExtra("ratting").toString()
                searchmap["theme_id"] = getString(R.string.theme_id)
                callsearchApi(searchmap)

            }
        }

}