package com.workdo.glasswear.utils;

import com.workdo.glasswear.model.CountryDataItem;

public interface OnItemClickListenerGuestCountry {
    void onItemClickGuest(CountryDataItem item);
}
