package com.workdo.glasswear.utils;

import com.workdo.glasswear.model.CityListData;

public interface OnItemClickListenerGuestCity {
    void onItemClickCityGuest(CityListData item);
}
