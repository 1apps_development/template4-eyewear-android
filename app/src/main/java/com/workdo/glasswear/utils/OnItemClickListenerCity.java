package com.workdo.glasswear.utils;

import com.workdo.glasswear.model.CityListData;

public interface OnItemClickListenerCity {
    void onItemClickCity(CityListData item);
}
