package com.workdo.glasswear.utils;

import com.workdo.glasswear.model.CountryDataItem;

public interface OnItemClickListenerCountry {
    void onItemClick(CountryDataItem item);
}
