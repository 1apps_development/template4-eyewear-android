package com.workdo.glasswear.utils;

import com.workdo.glasswear.model.StateListData;

public interface OnItemClickListenerGuestState {
    void onItemClickStateGuest(StateListData item);
}
